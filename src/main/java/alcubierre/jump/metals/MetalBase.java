package alcubierre.jump.metals;

import net.minecraft.block.material.MapColor;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.SoundEvent;

/**
 * Created by audiomodder on 8/15/2016.
 */
public class MetalBase
{

   private String name;
   private boolean isOre;
   private int meta;
   private int blockLvl;
   private int veinsPerChunk;
   private int oresPerVein;
   private int minGenLevel;
   private int maxGenLevel;
   private int veinChance;
   private int veinDensity;
   private String dimensions;

   //Tool Material properties
   private int harvestLevel;
   private int maxUses;
   private float efficiency;
   private float damage;
   private int toolEnchantability;

   //Armor Material Properties
   private int durability;
   private int reductionAmountHead;
   private int reductionAmountChest;
   private int reductionAmountLegs;
   private int reductionAmountFeet;
   private int armorEnchantability;
   private SoundEvent soundOnEquip;
   private float toughness;

   public String name()
   {
      return name;
   }
   public boolean isOre()
   {
      return isOre;
   }
   public int meta()
   {
      return meta;
   }
   public int blockLvl()
   {
      return blockLvl;
   }
   public String alloyRecipe()
   {
      return "";
   }
   public int veinsPerChunk()
   {
      return veinsPerChunk;
   };
   public int oresPerVein()
   {
      return oresPerVein;
   };
   public int minGenLevel()
   {
      return minGenLevel;
   };
   public int maxGenLevel()
   {
      return maxGenLevel;
   };
   public int veinChance()
   {
      return veinChance;
   };
   public int veinDensity()
   {
      return veinDensity;
   };
   public String dimensions()
   {
      return dimensions;
   }
   public int harvestLevel()
   {
      return harvestLevel;
   }
   public int maxUses()
   {
      return maxUses;
   }
   public float efficiency()
   {
      return efficiency;
   }
   public float damage()
   {
      return damage;
   }
   public int toolEnchantability()
   {
      return toolEnchantability;
   }
   public int durability()
   {
      return durability;
   }
   public int reductionAmountHead()
   {
      return reductionAmountHead;
   }
   public int reductionAmountChest()
   {
      return reductionAmountChest;
   }
   public int reductionAmountLegs()
   {
      return reductionAmountLegs;
   }
   public int reductionAmountFeet()
   {
      return reductionAmountFeet;
   }
   public int armorEnchantability()
   {
      return armorEnchantability;
   }
   public SoundEvent soundOnEquip()
   {
      return SoundEvents.ITEM_ARMOR_EQUIP_IRON;
   }
   public float toughness()
   {
      return toughness;
   }
}