package alcubierre.jump.metals.blocks;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by audiomodder on 8/18/2016.
 */
public class MetalOre extends BlockOre
{
   private MetalBase metal;

   public MetalOre(MetalBase metalBase)
   {
      super();
      setUnlocalizedName(ModInformation.ID + "." + metalBase.name() + "Ore");
      this.setRegistryName(ModInformation.ID, metalBase.name() + "Ore");
      setCreativeTab(Jump.CREATIVE_TAB);
      setHardness(3.5f);
      metal = metalBase;

      MinecraftForge.EVENT_BUS.register(this);
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Ore"));
   }
}
