package alcubierre.jump.metals.blocks;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by audiomodder on 8/18/2016.
 */
public class MetalBlock extends Block
{
   private MetalBase metal;

   public MetalBlock(MetalBase metalBase)
   {
      super(new Material(MapColor.GREEN));
      setUnlocalizedName(ModInformation.ID + "." + metalBase.name() + "Block");
      this.setRegistryName(ModInformation.ID, metalBase.name() + "Block");
      setCreativeTab(Jump.CREATIVE_TAB);
      setHardness(3.5f);
      metal = metalBase;

      MinecraftForge.EVENT_BUS.register(this);
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Block"));
   }
}
