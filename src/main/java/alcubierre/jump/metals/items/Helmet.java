package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by audiomodder on 8/15/2016.
 */
public class Helmet extends ItemArmor
{
   private MetalBase metal;

   public Helmet(ArmorMaterial armorMaterial, MetalBase baseMetal)
   {
      super(armorMaterial, 1, EntityEquipmentSlot.HEAD);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Helmet");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Helmet");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Helmet"));
   }

   @Override
   public String getArmorTexture(ItemStack itemStack, Entity entity, EntityEquipmentSlot slot, String layer)
   {
      return ModInformation.ID +  ":textures/onbodyarmor/" + metal.name() + "1.png";
   }
}
