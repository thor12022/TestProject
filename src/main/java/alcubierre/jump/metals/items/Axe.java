package alcubierre.jump.metals.items;

import net.minecraftforge.client.model.ModelLoader;
import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemAxe;


public class Axe extends ItemAxe
{
   private MetalBase metal;

   public Axe(ToolMaterial toolMaterial, MetalBase baseMetal)
   {
      super(toolMaterial, 1, 1);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Axe");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Axe");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Axe"));
   }
}
