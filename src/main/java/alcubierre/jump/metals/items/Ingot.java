package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import alcubierre.jump.util.Utils;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by audiomodder on 8/15/2016.
 */
public class Ingot extends Item
{
   private MetalBase metal;

   public Ingot(MetalBase baseMetal)
   {
      super();
      this.setUnlocalizedName(ModInformation.ID + "." + Utils.CapFirstLetter(baseMetal.name()) + " Ingot");
      this.setRegistryName(ModInformation.ID, "ingot" + Utils.CapFirstLetter(baseMetal.name()));
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Ingot"));
   }
}
