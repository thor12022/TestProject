package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by audiomodder on 8/15/2016.
 */
public class Nugget extends Item
{
   private MetalBase metal;

   public Nugget(MetalBase baseMetal)
   {
      super();
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Nugget");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Nugget");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Nugget"));
   }
}
