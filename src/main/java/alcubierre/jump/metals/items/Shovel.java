package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemSpade;
import net.minecraftforge.client.model.ModelLoader;

public class Shovel extends ItemSpade
{
   private MetalBase metal;

   public Shovel(ToolMaterial toolMaterial, MetalBase baseMetal)
   {
      super(toolMaterial);

      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Shovel");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Shovel");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Shovel"));
   }
}
