package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.client.model.ModelLoader;

public class Pickaxe extends ItemPickaxe
{
   private MetalBase metal;

   public Pickaxe(ToolMaterial toolMaterial, MetalBase baseMetal)
   {
      super(toolMaterial);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Pickaxe");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Pickaxe");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Pickaxe"));
   }
}
