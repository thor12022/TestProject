package alcubierre.jump.metals.items;

import java.util.Random;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.client.model.ModelLoader;

public class Sword extends ItemSword
{

   private int effectId = 0;
   private int effectDura = 0;
   private int effectAmp = 0;
   private int effectReceiver = 0;
   private MetalBase metal;

   public Sword(ToolMaterial toolMaterial, MetalBase baseMetal)
   {
      super(toolMaterial);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Sword");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Sword");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Sword"));
   }
}
