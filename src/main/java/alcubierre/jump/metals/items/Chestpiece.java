package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by audiomodder on 8/15/2016.
 */
public class Chestpiece extends ItemArmor
{
   private MetalBase metal;

   public Chestpiece(ArmorMaterial armorMaterial, MetalBase baseMetal)
   {
      super(armorMaterial, 1, EntityEquipmentSlot.CHEST);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Chestpiece");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Chestpiece");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Chestpiece"));
   }

   @Override
   public String getArmorTexture(ItemStack itemStack, Entity entity, EntityEquipmentSlot slot, String layer)
   {
      return ModInformation.ID +  ":textures/onbodyarmor/" + metal.name() + "1.png";
   }
}
