package alcubierre.jump.metals.items;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;
import alcubierre.jump.metals.MetalBase;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemHoe;
import net.minecraftforge.client.model.ModelLoader;


public class Hoe extends ItemHoe
{
   private MetalBase metal;

   public Hoe(ToolMaterial toolMaterial, MetalBase baseMetal)
   {
      super(toolMaterial);
      this.setUnlocalizedName(ModInformation.ID + "." + baseMetal.name() + "Hoe");
      this.setRegistryName(ModInformation.ID, baseMetal.name() + "Hoe");
      setCreativeTab(Jump.CREATIVE_TAB);
      metal = baseMetal;
   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + metal.name() + "Hoe"));
   }
}
