package alcubierre.jump.metals;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.IWorldGenerator;

import alcubierre.jump.metals.blocks.MetalOre;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class MetalsWorldGen implements IWorldGenerator {

   private static Map<String, MetalOre> metalOreList;
   private static Map<String, MetalBase> metalList;
   private static Map<String, WorldGenMinable> metalGeneratorList;

   public MetalsWorldGen()
   {
      MinecraftForge.EVENT_BUS.register(this);
      GameRegistry.registerWorldGenerator(this, 36);
      metalList = new HashMap<>();
      metalOreList = new HashMap<>();
      metalGeneratorList = new HashMap<>();
   }

   public void AddOre(MetalOre metalOre, MetalBase baseMetal)
   {
      metalOreList.put(baseMetal.name(), metalOre);
      metalList.put(baseMetal.name(), baseMetal);

      IBlockState metalBlockState = metalOre.getBlockState().getBaseState();
      WorldGenMinable generator = new WorldGenMinable(metalBlockState, 36);
      metalGeneratorList.put(baseMetal.name(), generator);
   }

   @Override
   public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
   {
      generateWorld(random, chunkX, chunkZ, world);
   }

   public void retroGen(Random random, int chunkX, int chunkZ, World world)
   {
      generateWorld(random, chunkX, chunkZ, world);
      world.getChunkFromChunkCoords(chunkX, chunkZ).setChunkModified();
   }

   private void generateWorld(Random random, int chunkX, int chunkZ, World world)
   {
      // shift to world coordinates
      int x = chunkX << 4;
      int y = chunkZ << 4;

      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         WorldGenMinable metalGenerator = metalGeneratorList.get(metalEntry.name());

         for (int i = 0; i < metalEntry.veinDensity(); i++)
         {
            int randPosX = x + random.nextInt(16);
            int randPosY = metalEntry.minGenLevel() + random.nextInt(metalEntry.maxGenLevel() - metalEntry.minGenLevel());
            int randPosZ = y + random.nextInt(16);

            metalGenerator.generate(world, random, new BlockPos(randPosX, randPosY, randPosZ));
         }
      }
   }

}
