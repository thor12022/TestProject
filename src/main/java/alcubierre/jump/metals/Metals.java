package alcubierre.jump.metals;

import alcubierre.jump.util.Logging;
import alcubierre.jump.util.Utils;
import com.google.gson.Gson;

import alcubierre.jump.Jump;
import alcubierre.jump.Mw;
import alcubierre.jump.metals.blocks.MetalBlock;
import alcubierre.jump.metals.blocks.MetalOre;
import alcubierre.jump.metals.items.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class Metals
{
   final private static Map<String, MetalBase> metalList = new HashMap<>();

   final private static Map<String, Ingot> ingotList = new HashMap<>();
   final private static Map<String, Nugget> nuggetList = new HashMap<>();
   final private static Map<String, Dust> dustList = new HashMap<>();

   final private static Map<String, Item.ToolMaterial> metalToolMaterialList = new HashMap<>();
   final private static Map<String, Shovel> metalToolShovel = new HashMap<>();
   final private static Map<String, Axe> metalToolAxe = new HashMap<>();
   final private static Map<String, Pickaxe> metalToolPickaxe = new HashMap<>();
   final private static Map<String, Hoe> metalToolHoe = new HashMap<>();
   final private static Map<String, Sword> metalToolSword = new HashMap<>();

   final private static Map<String, ItemArmor.ArmorMaterial> metalArmorMaterialList = new HashMap<>();
   final private static Map<String, Helmet> metalArmorHelmet = new HashMap<>();
   final private static Map<String, Chestpiece> metalArmorChestpiece = new HashMap<>();
   final private static Map<String, Leggings> metalArmorLeggings = new HashMap<>();
   final private static Map<String, Boots> metalArmorBoots = new HashMap<>();

   final private static Map<String, MetalBlock> metalBlocks = new HashMap<>();
   final private static Map<String, Item> metalBlockItems = new HashMap<>();
   final private static Map<String, MetalOre> metalOres = new HashMap<>();

   private MetalsWorldGen generator;
    
   public Metals()
   {
      loadMetalList();
      generator = new MetalsWorldGen();
   }

   private void loadMetalList()
   {
      try
      {
         InputStream inputStream = Mw.class.getClass().getResourceAsStream("/assets/jump/data/metals.json");
         Reader reader = new InputStreamReader(inputStream, "UTF-8");
         MetalBase[] metals = null;
         metals = new Gson().fromJson(reader, MetalBase[].class);

         for (MetalBase metal : metals)
         {
            Jump.LOGGER.debug("PUTTING METAL:  " + metal.name());
            metalList.put(metal.name(), metal);
         }
         reader.close();
         inputStream.close();
      }
      catch (IOException e)
      {
         Jump.LOGGER.warn(e.getLocalizedMessage());
      }
   }

   public void createMetalItems()
   {
      Jump.LOGGER.info("Creating Metal Items");
      createItems();
      createMetalToolsMaterials();
      createMetalTools();
      createMetalArmorMaterials();
      createMetalArmors();
      createMetalBlocks();
   }

   public void registerMetalItems()
   {
      Jump.LOGGER.info("Registering Metal Items");
      registerItems();
      registerMetalTools();
      registerMetalArmors();
      registerMetalBlocks();
   }

   public void registerMetalRecipes()
   {
      Jump.LOGGER.info("Registering Metal Recipes");
      craftingMetalItems();
      craftingMetalBlocks();
      craftingMetalTools();
      craftingMetalArmors();
      smeltingMetal();
   }

   public Item getIngot(String name)
   {
      return ingotList.get(name);
   }

   public Item getNugget(String name)
   {
      return nuggetList.get(name);
   }

   public Item getBlock(String name)
   {
      return metalBlockItems.get(name);
   }

/* *********************************************START PRIVATE SUBS********************************************* */
   private void createItems()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();

         Ingot metalIngot = new Ingot(metalEntry);
         ingotList.put(metalEntry.name(), metalIngot);
         GameRegistry.register(metalIngot);

         Nugget metalNugget = new Nugget(metalEntry);
         nuggetList.put(metalEntry.name(), metalNugget);
         GameRegistry.register(metalNugget);

         Dust metalDust = new Dust(metalEntry);
         dustList.put(metalEntry.name(), metalDust);
         GameRegistry.register(metalDust);
      }
   }

   private void createMetalToolsMaterials()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         Item.ToolMaterial toolMaterial = EnumHelper.addToolMaterial(metalEntry.name(),  metalEntry.harvestLevel(),
                 metalEntry.maxUses(), metalEntry.efficiency(), metalEntry.damage(), metalEntry.toolEnchantability());

         metalToolMaterialList.put( metalEntry.name(), toolMaterial);

      }
   }

   private void createMetalArmorMaterials()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         int[] reductionAmounts = new int[] {metalEntry.reductionAmountFeet(), metalEntry.reductionAmountLegs(),
                 metalEntry.reductionAmountChest(), metalEntry.reductionAmountHead()};
         ItemArmor.ArmorMaterial armorMaterial = EnumHelper.addArmorMaterial(metalEntry.name(), metalEntry.name(),
                 metalEntry.durability(), reductionAmounts, metalEntry.armorEnchantability(), metalEntry.soundOnEquip(),
                 metalEntry.toughness());
         metalArmorMaterialList.put( metalEntry.name(), armorMaterial);
      }
   }

   private void createMetalTools()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();

         Shovel metalShovel = new Shovel(metalToolMaterialList.get(metalEntry.name()), metalEntry);
         metalToolShovel.put(metalEntry.name(), metalShovel);
         GameRegistry.register(metalShovel);

         Axe metalAxe = new Axe(metalToolMaterialList.get(metalEntry.name()), metalEntry);
         metalToolAxe.put(metalEntry.name(), metalAxe);
         GameRegistry.register(metalAxe);

         Pickaxe metalPickaxe = new Pickaxe(metalToolMaterialList.get(metalEntry.name()), metalEntry);
         metalToolPickaxe.put(metalEntry.name(), metalPickaxe);
         GameRegistry.register(metalPickaxe);

         Hoe metalHoe = new Hoe(metalToolMaterialList.get(metalEntry.name()), metalEntry);
         metalToolHoe.put(metalEntry.name(), metalHoe);
         GameRegistry.register(metalHoe);

         Sword metalSword = new Sword(metalToolMaterialList.get(metalEntry.name()), metalEntry);
         metalToolSword.put(metalEntry.name(), metalSword);
         GameRegistry.register(metalSword);

      }
   }

   private void createMetalArmors()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metal.getKey();

         Helmet metalHelmet = new Helmet(metalArmorMaterialList.get(metalName), metalEntry);
         metalArmorHelmet.put(metalName, metalHelmet);
         GameRegistry.register(metalHelmet);

         Chestpiece metalChestpiece = new Chestpiece(metalArmorMaterialList.get(metalName), metalEntry);
         metalArmorChestpiece.put(metalName, metalChestpiece);
         GameRegistry.register(metalChestpiece);

         Leggings metalLeggings = new Leggings(metalArmorMaterialList.get(metalName), metalEntry);
         metalArmorLeggings.put(metalName, metalLeggings);
         GameRegistry.register(metalLeggings);

         Boots metalBoots = new Boots(metalArmorMaterialList.get(metalName), metalEntry);
         metalArmorBoots. put(metalName, metalBoots);
         GameRegistry.register(metalBoots);
      }
   }

   private void createMetalBlocks()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         MetalBlock metalBlock = new MetalBlock(metalEntry);
         metalBlocks.put(metalEntry.name(), metalBlock);
         GameRegistry.register(metalBlock);
         Item itemBlock = new ItemBlock(metalBlock);
         itemBlock.setRegistryName((metalBlock.getRegistryName()));
         metalBlockItems.put(metalEntry.name(), itemBlock);
         GameRegistry.register(itemBlock);

         if(metalEntry.isOre())
         {
            MetalOre metalOre = new MetalOre(metalEntry);
            metalOres.put(metalEntry.name(), metalOre);
            GameRegistry.register(metalOre);
            GameRegistry.register(new ItemBlock(metalOre).setRegistryName(metalOre.getRegistryName()));
            OreDictionary.registerOre("ore" + Utils.CapFirstLetter(metalEntry.name()), metalOre);

            generator.AddOre(metalOre, metalEntry);
         }
      }
   }

   private void registerItems()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();

         ingotList.get(metalEntry.name()).registerModel();
         nuggetList.get(metalEntry.name()).registerModel();
         dustList.get(metalEntry.name()).registerModel();
      }
   }

   private void registerMetalTools()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();

         metalToolShovel.get(metalEntry.name()).registerModel();
         metalToolAxe.get(metalEntry.name()).registerModel();
         metalToolPickaxe.get(metalEntry.name()).registerModel();
         metalToolHoe.get(metalEntry.name()).registerModel();
         metalToolSword.get(metalEntry.name()).registerModel();
      }
   }

   private void registerMetalArmors()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();

         metalArmorHelmet.get(metalEntry.name()).registerModel();
         metalArmorChestpiece.get(metalEntry.name()).registerModel();
         metalArmorLeggings.get(metalEntry.name()).registerModel();
         metalArmorBoots.get(metalEntry.name()).registerModel();
      }
   }

   private void registerMetalBlocks()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         metalBlocks.get(metalEntry.name()).registerModel();

         if(metalEntry.isOre())
         {
            metalOres.get(metalEntry.name()).registerModel();
         }
      }
   }

   private void craftingMetalItems()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metalEntry.name();
         ItemStack ingot = new ItemStack(ingotList.get(metalName), 1);
         GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(nuggetList.get(metalName), 9), ingot));
         GameRegistry.addRecipe(new ShapedOreRecipe(ingot, "nnn", "nnn", "nnn", 'n', nuggetList.get(metalName)));
      }
   }

   private void craftingMetalTools()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metalEntry.name();
         Item ingot = ingotList.get(metalName);
         GameRegistry.addRecipe(new ShapedOreRecipe(metalToolShovel.get(metalName), " i ", " s ", " s ",
                 'i', ingot, 's', "stickWood"));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalToolHoe.get(metalName), "ii ", " s ", " s ",
                 'i', ingot, 's', "stickWood"));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalToolAxe.get(metalName), "ii ", "is ", " s ",
                 'i', ingot, 's', "stickWood"));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalToolPickaxe.get(metalName), "iii", " s ", " s ",
                 'i', ingot, 's', "stickWood"));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalToolSword.get(metalName), " i ", " i ", " s ",
                 'i', ingot, 's', "stickWood"));
      }
   }

   private void craftingMetalArmors()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metalEntry.name();
         Item ingot = ingotList.get(metalName);
         GameRegistry.addRecipe(new ShapedOreRecipe(metalArmorHelmet.get(metalName), "iii", "i i", "   ",
                 'i', ingot));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalArmorChestpiece.get(metalName), "i i", "iii", "iii",
                 'i', ingot));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalArmorLeggings.get(metalName), "iii", "i i", "i i",
                 'i', ingot));
         GameRegistry.addRecipe(new ShapedOreRecipe(metalArmorBoots.get(metalName), "i i", "i i", "   ",
                 'i', ingot));
      }

   }

   private void craftingMetalBlocks()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metalEntry.name();
         ItemStack block = new ItemStack(metalBlocks.get(metalName), 1);
         GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ingotList.get(metalName), 9), block));
         GameRegistry.addRecipe(new ShapedOreRecipe(block, "iii", "iii", "iii", 'i', ingotList.get(metalName)));
      }
   }

   private void smeltingMetal()
   {
      for(Map.Entry<String, MetalBase> metal : metalList.entrySet())
      {
         MetalBase metalEntry = metal.getValue();
         String metalName = metalEntry.name();
         ItemStack ingots = new ItemStack(ingotList.get(metalName), 1);
         GameRegistry.addSmelting(dustList.get(metalName), ingots, 0.7F);
         GameRegistry.addSmelting(metalOres.get(metalName), ingots, 0.7F);
      }
   }
}
