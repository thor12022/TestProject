package alcubierre.jump.client;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import alcubierre.jump.Jump;
import alcubierre.jump.Mw;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class MwKeyHandler
{
	public static KeyBinding keyMapGui = new KeyBinding("key.mw_open_gui", Keyboard.KEY_M, "Mapwriter");
	public static KeyBinding keyZoomIn = new KeyBinding("key.mw_zoom_in", Keyboard.KEY_PRIOR, "Mapwriter");
	public static KeyBinding keyZoomOut = new KeyBinding("key.mw_zoom_out", Keyboard.KEY_NEXT, "Mapwriter");

	public final KeyBinding[] keys =
	{
			keyMapGui,
			keyZoomIn,
			keyZoomOut
	};

	public MwKeyHandler()
	{
		ArrayList<String> listKeyDescs = new ArrayList<>();
		// Register bindings
		for (KeyBinding key : this.keys)
		{
			if (key != null)
			{
				ClientRegistry.registerKeyBinding(key);
	         listKeyDescs.add(key.getKeyDescription());
			}
		}
	}

	@SubscribeEvent
	public void keyEvent(InputEvent.KeyInputEvent event)
	{
		this.checkKeys();
	}

	private void checkKeys()
	{
		for (KeyBinding key : this.keys)
		{
			if ((key != null) && key.isPressed())
			{
			   Jump.API.onKeyDown(key);
			}
		}
	}
}
