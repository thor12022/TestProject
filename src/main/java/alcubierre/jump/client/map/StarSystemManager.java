package alcubierre.jump.client.map;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import alcubierre.jump.Jump;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.IStarSystem;
import alcubierre.jump.client.map.mapmode.MapMode;
import alcubierre.jump.config.WorldConfig;
import alcubierre.jump.galaxy.StarSystem;
import alcubierre.jump.util.Logging;
import alcubierre.jump.util.Reference;
import alcubierre.jump.util.Render;
import alcubierre.jump.util.Utils;
import net.minecraftforge.common.config.Configuration;

public class StarSystemManager
{
   final static private int SYSTEM_BORDER_COLOR = 0xFF000000;
   final static private int SELECTED_SYSTEM_BORDER_COLOR = 0xFFFFFFFF;
   final static private int CURRENT_SYSTEM_BORDER_COLOR = 0xFFFF0000;
   
   public static void drawSystem(StarSystem system, MapMode mapMode, MapView mapView, int borderColour)
   {
      //! @todo marker scaling?
      Point.Double p = mapMode.getClampedScreenXY(mapView, system.getPosition() );
      system.screenPos.setLocation(
            p.x + mapMode.getXTranslation(),
            p.y + mapMode.getYTranslation());

   // draw a coloured rectangle centered on the calculated (x, y)
      double mSize = mapMode.getConfig().markerSize;
      double halfMSize = mapMode.getConfig().markerSize / 2.0;
      Render.setColour(borderColour);
      Render.drawRect(p.x - halfMSize, p.y - halfMSize, mSize, mSize);
      Render.setColour(system.getColor());
      Render.drawRect((p.x - halfMSize) + 0.5, (p.y - halfMSize) + 0.5, mSize - 1.0, mSize - 1.0);
   }
   
   public List<StarSystem> visibleSystemList = new ArrayList<>();

   public StarSystem selectedSystem = null;
   public StarSystem currentSystem = null;

   public void update()
   {
      this.visibleSystemList.clear();
      Collection<IStarSystem> systems = Jump.API.getClientGalaxy().getSystems();
      systems.forEach(system -> visibleSystemList.add((StarSystem)system));
   }

   public void selectNextMarker()
   {
      if (this.visibleSystemList.size() > 0)
      {
         if (this.selectedSystem != null)
         {
            boolean next = false;
            for(IStarSystem system : this.visibleSystemList)
            {
               if(this.selectedSystem == system)
               {
                  next = true;
               }
               else if(next == true && system instanceof StarSystem)
               {
                  this.selectedSystem = (StarSystem)system;
                  break;
               }
            }
         }
      }
      else
      {
         this.selectedSystem = null;
      }
   }

   public StarSystem getNearestSystem(GalaxyVector pos, int maxDistance)
   {
      double nearestDistance = maxDistance * maxDistance;
      StarSystem nearestSystem = null;
      for (IStarSystem system : this.visibleSystemList)
      {
         double d = system.getPosition().distanceSq(pos);
         if (d < nearestDistance)
         {
            nearestSystem = (StarSystem)system;
            nearestDistance = d;
         }
      }
      return nearestSystem;
   }

   public void drawSystems(MapMode mapMode, MapView mapView)
   {
      for (StarSystem system : this.visibleSystemList)
      {
         drawSystem(system, mapMode, mapView, SYSTEM_BORDER_COLOR);
      }
      if (this.selectedSystem != null)
      {
         drawSystem(selectedSystem, mapMode, mapView, SELECTED_SYSTEM_BORDER_COLOR);
      }
      if (this.currentSystem != null)
      {
         drawSystem(currentSystem, mapMode, mapView, CURRENT_SYSTEM_BORDER_COLOR);
      }
   }
}
