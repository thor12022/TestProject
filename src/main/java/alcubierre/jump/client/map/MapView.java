package alcubierre.jump.client.map;

import alcubierre.jump.Mw;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.client.GalaxyMapDataProviderApi;
import alcubierre.jump.api.client.IMapMode;
import alcubierre.jump.api.client.IMapView;
import alcubierre.jump.config.ConfigData;

public class MapView implements IMapView
{
	private int zoomLevel = 0;
	private int textureSize = 2048;

	// the centerPos of the centre of the 'view' of the map using game (block)
	// coordinates
	GalaxyVector centerPos = GalaxyVector.ORIGIN;

	// width and height of map to display in pixels
	private int mapW = 0;
	private int mapH = 0;

	// the width and height of the map in blocks at zoom level 0.
	// updated when map width, map height, or texture size changes.
	private int baseW = 1;
	private int baseH = 1;

	// the width and height of the map in blocks at the current
	// zoom level.
	private double w = 1;
	private double h = 1;

	private int minZoom;
	private int maxZoom;

	private boolean fullscreenMap;

	public MapView(Mw mw, boolean FullscreenMap)
	{
		this.minZoom = ConfigData.zoomInLevels;
		this.maxZoom = ConfigData.zoomOutLevels;
		this.fullscreenMap = FullscreenMap;
		if (this.fullscreenMap)
		{
			this.setZoomLevel(ConfigData.fullScreenZoomLevel);
		}
		this.setZoomLevel(ConfigData.overlayZoomLevel);
		this.setViewCentre(mw.getPlayerGalaxyPos(mw.mc.thePlayer));
	}

	@Override
   public void setViewCentre(GalaxyVector pos)
	{
	   centerPos = pos;
	}
	
	@Override
   @Deprecated
   public double getX()
	{
		return this.centerPos.getX();
	}

   @Override
	@Deprecated
   public double getZ()
	{
		return this.centerPos.getZ();
	}


   @Override
   public GalaxyVector getCenter()
   {
      return centerPos;
   }
   
	@Override
   public double getWidth()
	{
		return this.w;
	}

	@Override
   public double getHeight()
	{
		return this.h;
	}

	@Override
   public void panView(double relX, double relZ)
	{
		this.centerPos.add(new GalaxyVector(relX * this.w, relZ * this.h));
		onUpdateCenter();
	}

	@Override
   public int setZoomLevel(int zoomLevel)
	{
		// MwUtil.log("MapView.setZoomLevel(%d)", zoomLevel);
		int prevZoomLevel = this.zoomLevel;
		this.zoomLevel = Math.min(Math.max(this.minZoom, zoomLevel), this.maxZoom);
		
		if (prevZoomLevel != this.zoomLevel)
		{
			this.updateZoom();
		}

		if (this.fullscreenMap)
		{
			ConfigData.fullScreenZoomLevel = this.zoomLevel;
		}
		ConfigData.overlayZoomLevel = this.zoomLevel;

		return this.zoomLevel;
	}
	
	private void onUpdateCenter()
	{
	   if (GalaxyMapDataProviderApi.getCurrentDataProvider() != null)
      {
         GalaxyMapDataProviderApi.getCurrentDataProvider().onMapCenterChanged(centerPos, this);
      }
	}

	private void updateZoom()
	{
		if (this.zoomLevel >= 0)
		{
			this.w = this.baseW << this.zoomLevel;
			this.h = this.baseH << this.zoomLevel;
		}
		else
		{
			this.w = this.baseW >> (-this.zoomLevel);
			this.h = this.baseH >> (-this.zoomLevel);
		}

		if (GalaxyMapDataProviderApi.getCurrentDataProvider() != null)
		{
			GalaxyMapDataProviderApi.getCurrentDataProvider().onZoomChanged(this.getZoomLevel(), this);
		}
	}

	@Override
   public int adjustZoomLevel(int n)
	{
		return this.setZoomLevel(this.zoomLevel + n);
	}

	@Override
   public int getZoomLevel()
	{
		return this.zoomLevel;
	}

	@Override
   public int getRegionZoomLevel()
	{
		return Math.max(0, this.zoomLevel);
	}

	// bX and bZ are the coordinates of the block the zoom is centred on.
	// The relative centerPos of the block in the view will remain the same
	// as before the zoom.
	@Override
   public void zoomToPoint(int newZoomLevel, GalaxyVector pos)
	{
		int prevZoomLevel = this.zoomLevel;
		newZoomLevel = this.setZoomLevel(newZoomLevel);
		double zF = Math.pow(2, newZoomLevel - prevZoomLevel);
		this.setViewCentre(pos.sub(pos.sub(this.centerPos).mult(zF)));
	}

	@Override
   public void setMapWH(int w, int h)
	{
		if ((this.mapW != w) || (this.mapH != h))
		{
			this.mapW = w;
			this.mapH = h;
			this.updateBaseWH();
		}
	}

	@Override
   public void setMapWH(IMapMode mapMode)
	{
		this.setMapWH(mapMode.getWPixels(), mapMode.getHPixels());
	}

	@Override
   public double getMinX()
	{
		return this.centerPos.getX() - (this.w / 2);
	}

	@Override
   public double getMaxX()
	{
		return this.centerPos.getX() + (this.w / 2);
	}

	@Override
   public double getMinZ()
	{
		return this.centerPos.getZ() - (this.h / 2);
	}

	@Override
   public double getMaxZ()
	{
		return this.centerPos.getZ() + (this.h / 2);
	}

	@Override
   public void setViewCentreScaled(GalaxyVector position)
	{
	   //! @todo scaling needed here?
		double scale = 1.0;
		this.setViewCentre(position.mult(scale));
	}

	@Override
   public void setTextureSize(int n)
	{
		if (this.textureSize != n)
		{
			this.textureSize = n;
			this.updateBaseWH();
		}
	}

	private void updateBaseWH()
	{
		int w = this.mapW;
		int h = this.mapH;
		int halfTextureSize = this.textureSize / 2;

		// if we cannot display the map at 1x1 pixel per block, then
		// try 2x2 pixels per block, then 4x4 and so on
		while ((w > halfTextureSize) || (h > halfTextureSize))
		{
			w /= 2;
			h /= 2;
		}

		// MwUtil.log("MapView.updateBaseWH: map = %dx%d, tsize = %d, base =
		// %dx%d",
		// this.mapW, this.mapH, this.textureSize, w, h);

		this.baseW = w;
		this.baseH = h;

		this.updateZoom();
	}

	@Override
   public int getPixelsPerBlock()
	{
		return this.mapW / this.baseW;
	}

	@Override
   public boolean isBlockWithinView(GalaxyVector pos, boolean circular)
	{
		boolean inside;
		if (!circular)
		{
			inside = (pos.getX() > this.getMinX()) || (pos.getX() < this.getMaxX()) || (pos.getZ() > this.getMinZ())
					|| (pos.getZ() < this.getMaxZ());
		}
		else
		{
			double r = this.getHeight() / 2;
			inside = pos.distanceSq(this.centerPos) < (r * r);
		}
		return inside;
	}
}
