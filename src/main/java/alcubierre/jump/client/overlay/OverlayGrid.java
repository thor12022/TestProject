package alcubierre.jump.client.overlay;

import java.util.ArrayList;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.client.ILabelInfo;
import alcubierre.jump.api.client.IMapMode;
import alcubierre.jump.api.client.IMapView;
import alcubierre.jump.api.client.IMwChunkOverlay;
import alcubierre.jump.api.client.IMwDataProvider;
import net.minecraft.util.math.MathHelper;

public class OverlayGrid implements IMwDataProvider
{
	public class ChunkOverlay implements IMwChunkOverlay
	{

		final private GalaxyVector position;

		ChunkOverlay(int x, int z)
		{
         this.position = new GalaxyVector(x, z);
      }

		@Override
		public GalaxyVector getCoordinates()
		{
			return this.position;
		}

		@Override
		public int getColor()
		{
			return 0x00ffffff;
		}

		@Override
		public float getFilling()
		{
			return 1.0f;
		}

		@Override
		public boolean hasBorder()
		{
			return true;
		}

		@Override
		public float getBorderWidth()
		{
			return 0.5f;
		}

		@Override
		public int getBorderColor()
		{
			return 0xff000000;
		}

	}

	@Override
	public ArrayList<IMwChunkOverlay> getChunksOverlay(GalaxyVector pos,
			double minX, double minZ, double maxX, double maxZ)
	{
		int minChunkX = (MathHelper.ceiling_double_int(minX) >> 4) - 1;
		int minChunkZ = (MathHelper.ceiling_double_int(minZ) >> 4) - 1;
		int maxChunkX = (MathHelper.ceiling_double_int(maxX) >> 4) + 1;
		int maxChunkZ = (MathHelper.ceiling_double_int(maxZ) >> 4) + 1;
		int cX = (MathHelper.ceiling_double_int(pos.getX()) >> 4) + 1;
		int cZ = (MathHelper.ceiling_double_int(pos.getZ()) >> 4) + 1;

		int limitMinX = Math.max(minChunkX, cX - 100);
		int limitMaxX = Math.min(maxChunkX, cX + 100);
		int limitMinZ = Math.max(minChunkZ, cZ - 100);
		int limitMaxZ = Math.min(maxChunkZ, cZ + 100);

		ArrayList<IMwChunkOverlay> chunks = new ArrayList<>();
		for (int x = limitMinX; x <= limitMaxX; x++)
		{
			for (int z = limitMinZ; z <= limitMaxZ; z++)
			{
				chunks.add(new ChunkOverlay(x, z));
			}
		}

		return chunks;
	}

	@Override
	public String getStatusString(GalaxyVector pos)
	{
		return "";
	}

	@Override
	public void onMiddleClick(GalaxyVector pos, IMapView mapview)
	{
	}

	@Override
	public void onMapCenterChanged(GalaxyVector deltaPos, IMapView mapview)
	{

	}

	@Override
	public void onZoomChanged(int level, IMapView mapview)
	{

	}

	@Override
	public void onOverlayActivated(IMapView mapview)
	{

	}

	@Override
	public void onOverlayDeactivated(IMapView mapview)
	{

	}

	@Override
	public void onDraw(IMapView mapview, IMapMode mapmode)
	{

	}

	@Override
	public boolean onMouseInput(IMapView mapview, IMapMode mapmode)
	{

		return false;
	}

	@Override
	public ILabelInfo getLabelInfo(int mouseX, int mouseY)
	{
		return null;
	}
}
