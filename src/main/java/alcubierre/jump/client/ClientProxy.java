package alcubierre.jump.client;

import alcubierre.jump.CommonProxy;
import alcubierre.jump.Jump;
import alcubierre.jump.ModInformation;
import alcubierre.jump.api.client.GalaxyMapDataProviderApi;
import alcubierre.jump.client.overlay.OverlayGrid;
import alcubierre.jump.util.Reference;
import alcubierre.jump.util.VersionCheck;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.FMLInterModComms;

public class ClientProxy extends CommonProxy
{
   @Override
   public void preInit()
   {
      super.preInit();

      Jump.metals.registerMetalItems();
      Jump.machines.registerItems();
   }

   @Override
   public void load()
   {
      super.load();
		    EventHandler eventHandler = new EventHandler(Jump.API);
	  	  MinecraftForge.EVENT_BUS.register(eventHandler);

	  	  MwKeyHandler keyEventHandler = new MwKeyHandler();
		    MinecraftForge.EVENT_BUS.register(keyEventHandler);
	  }

   @Override
   public void postInit()
   {
      super.postInit();
      if (Loader.isModLoaded("VersionChecker"))
      {
         FMLInterModComms.sendRuntimeMessage(ModInformation.ID, "VersionChecker", "addVersionCheck", Reference.VersionURL);
      }
      else
      {
         VersionCheck versionCheck = new VersionCheck();
         Thread versionCheckThread = new Thread(versionCheck, "Version Check");
         versionCheckThread.start();
      }

      GalaxyMapDataProviderApi.registerDataProvider("Grid", new OverlayGrid());

      Jump.API.initClient();
   }
}
