package alcubierre.jump.client;

import alcubierre.jump.Jump;
import alcubierre.jump.Mw;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandler
{

	Mw mw;

	public EventHandler(Mw mw)
	{
		this.mw = mw;
	}

	@SubscribeEvent
	public void renderMap(RenderGameOverlayEvent.Post event)
	{
		if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR)
		{
		   Jump.API.onTick();
		}
	}
}
