package alcubierre.jump.client.gui;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CreativeTabJump extends CreativeTabs {

    public CreativeTabJump(String tabLabel) {
        super(tabLabel);
        //setBackgroundImageName(ModInformation.ID + ".png"); // Automagically has tab_ applied to it. Make sure you change the texture name.
    }

    @Override
    public boolean hasSearchBar() {
        return true;
    }

    //The tab icon is what you return here.
    @Override
    public ItemStack getIconItemStack() {
        return new ItemStack(Items.IRON_INGOT);
    }

    @Override
    public Item getTabIconItem() {
        return new Item();
    }
}
