package alcubierre.jump.client.gui;

import alcubierre.jump.client.map.StarSystemManager;
import alcubierre.jump.galaxy.StarSystem;
import alcubierre.jump.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class MwGuiMarkerListOverlay extends MwGuiSlot
{
	private final GuiScreen parentScreen;
	private final StarSystemManager starSystemManager;

	public static int listWidth = 95;
	public static int ListY = 10;
	private int height;

	public MwGuiMarkerListOverlay(GuiScreen parentScreen, StarSystemManager starSystemManager)
	{
		super(Minecraft.getMinecraft(), // mcIn
				listWidth, // width
				parentScreen.height - 20, // height
				ListY, // topIn
				(10 + parentScreen.height) - 20, // bottomIn
				parentScreen.width - 110 // left
		);

		this.height = parentScreen.height - 20;

		this.parentScreen = parentScreen;
		this.starSystemManager = starSystemManager;
	}

	@Override
	protected int getSlotHeight(int index)
	{
		String str = Utils.stringArrayToString(this.getLabelString(index));
		int height = this.mc.fontRendererObj.splitStringWidth(str, MwGuiMarkerListOverlay.listWidth - 6);

		height += this.spacingY * 2;

		return height;
	}

	protected String[] getLabelString(int index)
	{
		StarSystem system = this.starSystemManager.visibleSystemList.get(index);

		String[] text = new String[2];
		text[0] = system.getName();
		text[1] = String.format("(%d, %d)", system.getPosition().getX(), system.getPosition().getZ());
		return text;
	}

	@Override
	protected int getSize()
	{
		return this.starSystemManager.visibleSystemList.size();
	}

	@Override
	protected void elementClicked(int slotIndex, boolean isDoubleClick, int mouseX, int mouseY, int mouseButton)
	{
		this.starSystemManager.selectedSystem = this.starSystemManager.visibleSystemList.get(slotIndex);
		if (mouseButton == 1)
		{
			if (this.parentScreen instanceof MwGui)
			{
				((MwGui) this.parentScreen).openMarkerGui(this.starSystemManager.selectedSystem, mouseX, mouseY);
				;
			}
		}
		if ((mouseButton == 0) && isDoubleClick)
		{
			if (this.parentScreen instanceof MwGui)
			{
				((MwGui) this.parentScreen).centerOnSelectedMarker();
			}
		}
	}

	@Override
	protected boolean isSelected(int slotIndex)
	{
		return this.starSystemManager.selectedSystem == this.starSystemManager.visibleSystemList.get(slotIndex);
	}

	@Override
	protected void drawBackground()
	{
	}

	@Override
	protected void drawSlot(int entryID, int x, int y, int slotHeight, int mouseXIn, int mouseYIn)
	{
		MwGuiLabel label = new MwGuiLabel(this.getLabelString(entryID), null, x, y, false, false, MwGuiMarkerListOverlay.listWidth, this.height);

		label.draw();
	}

	@Override
	public void setDimensions(int widthIn, int heightIn, int topIn, int bottomIn, int left)
	{
		this.height = this.parentScreen.height - 20;

		super.setDimensions(widthIn, heightIn, topIn, bottomIn, left);
	}
}
