package alcubierre.jump.client.gui;

import java.awt.Point;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Mw;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.IStarSystem;
import alcubierre.jump.api.client.GalaxyMapDataProviderApi;
import alcubierre.jump.api.client.ILabelInfo;
import alcubierre.jump.api.client.IMwDataProvider;
import alcubierre.jump.client.MwKeyHandler;
import alcubierre.jump.client.map.MapRenderer;
import alcubierre.jump.client.map.MapView;
import alcubierre.jump.client.map.mapmode.MapMode;
import alcubierre.jump.config.ConfigData;
import alcubierre.jump.galaxy.StarSystem;
import alcubierre.jump.util.Logging;
import alcubierre.jump.util.Reference;
import alcubierre.jump.util.Utils;
import alcubierre.jump.util.VersionCheck;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiConfirmOpenLink;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class MwGui extends GuiScreen
{
	private Mw mw;
	public MapMode mapMode;
	private MapView mapView;
	private MapRenderer map;

	private String[] HelpText1 = new String[]
	{
			"mw.gui.mwgui.keys",
			"",
			"  Space",
			"  Delete",
			"  C",
			"  Home",
			"  End",
			"  N",
			"  T",
			"  P",
			"  R",
			"  U",
			"  L",
			"",
			"mw.gui.mwgui.helptext.1",
			"mw.gui.mwgui.helptext.2",
			"mw.gui.mwgui.helptext.3",
			"mw.gui.mwgui.helptext.4",
			"mw.gui.mwgui.helptext.5",
			"mw.gui.mwgui.helptext.6",
			"",
			"mw.gui.mwgui.helptext.7",
			"mw.gui.mwgui.helptext.8",
			"mw.gui.mwgui.helptext.9"
	};
	private String[] HelpText2 = new String[]
	{
			"",
			"",
			"mw.gui.mwgui.helptext.nextmarkergroup",
			"mw.gui.mwgui.helptext.deletemarker",
			"mw.gui.mwgui.helptext.cyclecolour",
			"mw.gui.mwgui.helptext.centermap",
			"mw.gui.mwgui.helptext.centermapplayer",
			"mw.gui.mwgui.helptext.selectnextmarker",
			"mw.gui.mwgui.helptext.teleport",
			"mw.gui.mwgui.helptext.savepng",
			"mw.gui.mwgui.helptext.regenerate",
			"mw.gui.mwgui.helptext.undergroundmap",
			"mw.gui.mwgui.helptext.markerlist"
	};

	private final static double PAN_FACTOR = 0.3D;

	private static final int menuY = 5;
	private static final int menuX = 5;

	private int mouseLeftHeld = 0;
	private int mouseLeftDragStartX = 0;
	private int mouseLeftDragStartY = 0;
	
	private GalaxyVector viewPosStart = GalaxyVector.ORIGIN;
	private GalaxyVector mouseGalaxyPos = GalaxyVector.ORIGIN;

	private MwGuiLabel helpLabel;
	private MwGuiLabel optionsLabel;
	private MwGuiLabel overlayLabel;
	private MwGuiLabel updateLabel;
	private MwGuiMarkerListOverlay MarkerOverlay;

	private MwGuiLabel helpTooltipLabel;
	private MwGuiLabel updateTooltipLabel;
	private MwGuiLabel statusLabel;
	private MwGuiLabel markerLabel;

	public static MwGui instance;

	private URI clickedLinkURI;

	public MwGui(Mw mw)
	{
		this.mw = mw;
		this.mapMode = new MapMode(ConfigData.fullScreenMap);
		this.mapView = new MapView(this.mw, true);
		this.map = new MapRenderer(this.mw, this.mapMode, this.mapView);

		//! @todo A better way to get the player from the GUI
		this.mapView.setViewCentreScaled(mw.getPlayerGalaxyPos(mw.mc.thePlayer));
		this.mapView.setZoomLevel(ConfigData.fullScreenZoomLevel);

		this.initLabels();

		this.MarkerOverlay = new MwGuiMarkerListOverlay(this, this.mw.starSystemManager);

		instance = this;
		
		mw.starSystemManager.update();
	}

	// called when gui is displayed and every time the screen
	// is resized
	@Override
	public void initGui()
	{
		this.helpLabel.setParentWidthAndHeight(this.width, this.height);
		this.optionsLabel.setParentWidthAndHeight(this.width, this.height);
		this.overlayLabel.setParentWidthAndHeight(this.width, this.height);
		this.updateLabel.setParentWidthAndHeight(this.width, this.height);

		this.helpTooltipLabel.setParentWidthAndHeight(this.width, this.height);
		this.updateTooltipLabel.setParentWidthAndHeight(this.width, this.height);
		this.statusLabel.setParentWidthAndHeight(this.width, this.height);
		this.markerLabel.setParentWidthAndHeight(this.width, this.height);

		this.MarkerOverlay.setDimensions(
				MwGuiMarkerListOverlay.listWidth,
				this.height - 20,
				MwGuiMarkerListOverlay.ListY,
				(10 + this.height) - 20,
				this.width - 110);
	}

	public void initLabels()
	{
		this.helpLabel = new MwGuiLabel(new String[]
		{
				"[" + I18n.format("mw.gui.mwgui.help") + "]"
		}, null, menuX, menuY, true, false, this.width, this.height);
		this.optionsLabel = new MwGuiLabel(new String[]
		{
				"[" + I18n.format("mw.gui.mwgui.options") + "]"
		}, null, 0, 0, true, false, this.width, this.height);
		this.overlayLabel = new MwGuiLabel(null, null, 0, 0, true, false, this.width, this.height);
		String updateString = "[" + I18n.format(
				"mw.gui.mwgui.newversion",
				VersionCheck.getLatestVersion()) + "]";
		this.updateLabel = new MwGuiLabel(new String[]
		{
				updateString
		}, null, 0, 0, true, false, this.width, this.height);
		this.helpTooltipLabel = new MwGuiLabel(
				this.HelpText1,
				this.HelpText2,
				0,
				0,
				true,
				false,
				this.width,
				this.height);

		this.updateTooltipLabel = new MwGuiLabel(new String[]
		{
				VersionCheck.getUpdateURL()
		}, null, 0, 0, true, false, this.width, this.height);

		this.statusLabel = new MwGuiLabel(null, null, 0, 0, true, false, this.width, this.height);
		this.markerLabel = new MwGuiLabel(null, null, 0, 0, true, true, this.width, this.height);

		this.optionsLabel.drawToRightOf(this.helpLabel);
		this.overlayLabel.drawToRightOf(this.optionsLabel);
		this.updateLabel.drawToRightOf(this.overlayLabel);

		this.helpTooltipLabel.drawToBelowOf(this.helpLabel);
		this.updateTooltipLabel.drawToBelowOf(this.helpLabel);
	}

	// called when a button is pressed
	@Override
	protected void actionPerformed(GuiButton button)
	{
	}

   // get a marker near the specified block pos if it exists.
   // the maxDistance is based on the view width so that you need to click
   // closer
   // to a marker when zoomed in to select it.
   public StarSystem getStarSystemNearScreenPos(int x, int y)
   {
      StarSystem nearSystem = null;
      for (StarSystem system : this.mw.starSystemManager.visibleSystemList)
      {
         if (system.screenPos != null)
         {
            if (system.screenPos.distanceSq(x, y) < 6.0)
            {
               nearSystem = system;
            }
         }
      }
      return nearSystem;
   }

	public boolean isPlayerNearScreenPos(int x, int y)
	{
		Point.Double p = this.map.playerArrowScreenPos;
		return p.distanceSq(x, y) < 9.0;
	}

	public void regenerateView()
	{
		Utils.printBoth(
				I18n.format(
						"mw.gui.mwgui.chatmsg.regenmap",
						(int) this.mapView.getWidth(),
						(int) this.mapView.getHeight(),
						(int) this.mapView.getMinX(),
						(int) this.mapView.getMinZ()));
	}

	// c is the ascii equivalent of the key typed.
	// key is the lwjgl key code.
	@Override
	protected void keyTyped(char c, int key)
	{
		// MwUtil.log("MwGui.keyTyped(%c, %d)", c, key);
		switch (key)
		{
		case Keyboard.KEY_ESCAPE:
			this.exitGui();
			break;

		case Keyboard.KEY_N:
			// select next visible marker
			this.mw.starSystemManager.selectNextMarker();
			break;

		case Keyboard.KEY_HOME:
			// centre map on player
		   //! @todo A better way to get the player from the GUI
	      this.mapView.setViewCentreScaled(mw.getPlayerGalaxyPos(mw.mc.thePlayer));
			break;

		case Keyboard.KEY_END:
			// centre map on selected marker
			this.centerOnSelectedMarker();
			break;

		case Keyboard.KEY_T:
			if (this.mw.starSystemManager.selectedSystem != null)
			{
				this.mw.teleportToSystem(this.mw.starSystemManager.selectedSystem);
				this.exitGui();
			}
         break;
		case Keyboard.KEY_LEFT:
			this.mapView.panView(-PAN_FACTOR, 0);
			break;
		case Keyboard.KEY_RIGHT:
			this.mapView.panView(PAN_FACTOR, 0);
			break;
		case Keyboard.KEY_UP:
			this.mapView.panView(0, -PAN_FACTOR);
			break;
		case Keyboard.KEY_DOWN:
			this.mapView.panView(0, PAN_FACTOR);
			break;

		case Keyboard.KEY_R:
			this.regenerateView();
			this.exitGui();
			break;

		case Keyboard.KEY_L:
			this.MarkerOverlay.setEnabled(!this.MarkerOverlay.getEnabled());
			break;

		default:
			if (key == MwKeyHandler.keyMapGui.getKeyCode())
			{
				this.exitGui();
			}
			else if (key == MwKeyHandler.keyZoomIn.getKeyCode())
			{
				this.mapView.adjustZoomLevel(-1);
			}
			else if (key == MwKeyHandler.keyZoomOut.getKeyCode())
			{
				this.mapView.adjustZoomLevel(1);
			}
			break;
		}
	}

	// override GuiScreen's handleMouseInput to process
	// the scroll wheel.
	@Override
	public void handleMouseInput() throws IOException
	{
		if (this.MarkerOverlay.isMouseInField() && (this.mouseLeftHeld == 0))
		{
			this.MarkerOverlay.handleMouseInput();
		}
		else if ((GalaxyMapDataProviderApi.getCurrentDataProvider() != null) && GalaxyMapDataProviderApi
				.getCurrentDataProvider()
				.onMouseInput(this.mapView, this.mapMode))
		{
			return;
		}
		else
		{
			int x = (Mouse.getEventX() * this.width) / this.mc.displayWidth;
			int y = this.height - ((Mouse.getEventY() * this.height) / this.mc.displayHeight) - 1;
			int direction = Mouse.getEventDWheel();
			if (direction != 0)
			{
				this.mouseDWheelScrolled(x, y, direction);
			}
		}
		super.handleMouseInput();
	}

	// mouse button clicked. 0 = LMB, 1 = RMB, 2 = MMB
	@Override
	protected void mouseClicked(int x, int y, int button)
	{
		StarSystem marker = this.getStarSystemNearScreenPos(x, y);

		if (this.MarkerOverlay.isMouseInField() && (this.mouseLeftHeld == 0))
		{
			this.MarkerOverlay.handleMouseInput();
		}
		else
		{
			if (button == 0)
			{
				if(this.optionsLabel.posWithin(x, y))
				{
					try
					{
						GuiScreen newScreen = ModGuiConfig.class
								.getConstructor(GuiScreen.class)
								.newInstance(this);
						this.mc.displayGuiScreen(newScreen);
					}
					catch (Exception e)
					{
						Logging.logError(
								"There was a critical issue trying to build the config GUI for %s",
								ModInformation.ID);
					}
				}
				else if (this.updateLabel.posWithin(x, y))
				{
					URI uri;

					if (!this.mc.gameSettings.chatLinks)
					{
						return;
					}

					try
					{
						uri = new URI(VersionCheck.getUpdateURL());

						if (!Reference.PROTOCOLS.contains(uri.getScheme().toLowerCase()))
						{
							throw new URISyntaxException(
									uri.toString(),
									"Unsupported protocol: " + uri.getScheme().toLowerCase());
						}

						if (this.mc.gameSettings.chatLinksPrompt)
						{
							this.clickedLinkURI = uri;
							this.mc.displayGuiScreen(
									new GuiConfirmOpenLink(this, uri.toString(), 31102009, false));
						}
						else
						{
							Utils.openWebLink(uri);
						}
					}
					catch (URISyntaxException urisyntaxexception)
					{
						Logging.logError("Can\'t open url for %s", urisyntaxexception);
					}
				}
			}
			else if (button == 1)
			{
				this.openMarkerGui(marker, x, y);
			}

			else if (button == 2)
			{
				GalaxyVector pos = this.mapMode.screenXYtoGalaxyPos(this.mapView, x, y);

				IMwDataProvider provider = GalaxyMapDataProviderApi.getCurrentDataProvider();
				if (provider != null)
				{
					provider.onMiddleClick(pos, this.mapView);
				}
			}

			this.viewPosStart = this.mapView.getCenter();
			// this.viewSizeStart = this.mapManager.getViewSize();
		}
	}

	// mouse button released. 0 = LMB, 1 = RMB, 2 = MMB
	// not called on mouse movement.
	@Override
	protected void mouseReleased(int x, int y, int button)
	{
		// MwUtil.log("MwGui.mouseMovedOrUp(%d, %d, %d)", x, y, button);
		if (button == 0)
		{
			this.mouseLeftHeld = 0;
		}
		else if (button == 1)
		{
			// this.mouseRightHeld = 0;
		}
	}

	// zoom on mouse direction wheel scroll
	public void mouseDWheelScrolled(int x, int y, int direction)
	{
	   StarSystem system = this.getStarSystemNearScreenPos(x, y);
		if (this.overlayLabel.posWithin(x, y))
		{
			int n = (direction > 0) ? 1 : -1;
			if (GalaxyMapDataProviderApi.getCurrentDataProvider() != null)
			{
				GalaxyMapDataProviderApi.getCurrentDataProvider().onOverlayDeactivated(this.mapView);
			}

			if (n == 1)
			{
				GalaxyMapDataProviderApi.setNextProvider();
			}
			else
			{
				GalaxyMapDataProviderApi.setPrevProvider();
			}

			if (GalaxyMapDataProviderApi.getCurrentDataProvider() != null)
			{
				GalaxyMapDataProviderApi.getCurrentDataProvider().onOverlayActivated(this.mapView);
			}

		}
		else
		{
			int zF = (direction > 0) ? -1 : 1;
			this.mapView.zoomToPoint(this.mapView.getZoomLevel() + zF, mouseGalaxyPos);
			ConfigData.fullScreenZoomLevel = this.mapView.getZoomLevel();
		}
	}

	// closes this gui
	public void exitGui()
	{
		this.mc.displayGuiScreen((GuiScreen) null);
	}

	/**
	 * Called when the screen is unloaded. Used to disable keyboard repeat
	 * events
	 */
	@Override
	public void onGuiClosed()
	{
		Keyboard.enableRepeatEvents(false);
	}

	// called every frame
	@Override
	public void updateScreen()
	{
	}

	public void drawStatus()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(I18n.format("mw.gui.mwgui.status.cursor", this.mouseGalaxyPos.getX(), this.mouseGalaxyPos.getZ()));
		

		IMwDataProvider provider = GalaxyMapDataProviderApi.getCurrentDataProvider();
		if (provider != null)
		{
			builder.append(provider.getStatusString(this.mouseGalaxyPos));
		}
		String s = builder.toString();
		int x = (this.width / 2) - 10 - (this.fontRendererObj.getStringWidth(s) / 2);

		this.statusLabel.setCoords(x, this.height - 21);
		this.statusLabel.setText(new String[]
		{
				builder.toString()
		}, null);
		this.statusLabel.draw();
	}

	// also called every frame
	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		this.drawDefaultBackground();
		double xOffset = 0.0;
		double yOffset = 0.0;
		// double zoomFactor = 1.0;

		if (this.mouseLeftHeld > 2)
		{
			xOffset = ((this.mouseLeftDragStartX - mouseX) * this.mapView.getWidth()) / this.mapMode
					.getW();
			yOffset = ((this.mouseLeftDragStartY - mouseY) * this.mapView.getHeight())
					/ this.mapMode.getH();

			this.mapView.setViewCentre(this.viewPosStart);
		}

		if (this.mouseLeftHeld > 0)
		{
			this.mouseLeftHeld++;
		}

		// draw the map
		this.map.draw();

		// let the renderEngine know we have changed the texture.
		// this.mc.renderEngine.resetBoundTexture();

		// get the block the mouse is currently hovering over
		this.mouseGalaxyPos = this.mapMode.screenXYtoGalaxyPos(this.mapView, mouseX, mouseY);

		// draw the label near mousepointer
		this.drawMarkerLabel(mouseX, mouseY, f);

		// draw status message
		this.drawStatus();

		// draw labels
		this.drawLabel(mouseX, mouseY, f);

		this.MarkerOverlay.drawScreen(mouseX, mouseY, f);
		;

		super.drawScreen(mouseX, mouseY, f);
	}

   @Override
   public boolean doesGuiPauseGame()
   {
       return false;
   }
	
	private void drawMarkerLabel(int mouseX, int mouseY, float f)
	{
		// draw name of marker under mouse cursor
	   StarSystem system = this.getStarSystemNearScreenPos(mouseX, mouseY);
		if (system != null)
		{
			this.markerLabel.setText(new String[]
			{
					system.getName(),
					String.format("(%d, %d)", (int)system.getPosition().getX(), (int)system.getPosition().getZ())
			}, null);
			this.markerLabel.setCoords(mouseX + 8, mouseY);
			this.markerLabel.draw();
			return;
		}

		IMwDataProvider provider = GalaxyMapDataProviderApi.getCurrentDataProvider();
		if (provider != null)
		{
			ILabelInfo info = provider.getLabelInfo(mouseX, mouseY);
			if (info != null)
			{
				this.markerLabel.setText(info.getInfoText(), null);
				this.markerLabel.setCoords(mouseX + 8, mouseY);
				this.markerLabel.draw();
				return;
			}
		}
	}

	private void drawLabel(int mouseX, int mouseY, float f)
	{
		this.helpLabel.draw();
		this.optionsLabel.draw();

		String overlayString = "[" + I18n.format(
				"mw.gui.mwgui.overlay",
				GalaxyMapDataProviderApi.getCurrentProviderName()) + "]";
		this.overlayLabel.setText(new String[]
		{
				overlayString
		}, null);
		this.overlayLabel.draw();

		if (!VersionCheck.isLatestVersion())
		{

			this.updateLabel.draw();
		}

		// help message on mouse over
		if (this.helpLabel.posWithin(mouseX, mouseY))
		{
			this.helpTooltipLabel.draw();
		}
		if (this.updateLabel.posWithin(mouseX, mouseY))
		{
			this.updateTooltipLabel.draw();
		}
	}

	@Override
	public void confirmClicked(boolean result, int id)
	{
		if (id == 31102009)
		{
			if (result)
			{
				Utils.openWebLink(this.clickedLinkURI);
			}

			this.clickedLinkURI = null;
			this.mc.displayGuiScreen(this);
		}
	}

	public void centerOnSelectedMarker()
	{
		if (this.mw.starSystemManager.selectedSystem != null)
		{
			this.mapView.setViewCentreScaled(this.mw.starSystemManager.selectedSystem.getPosition());
		}
	}

	public void openMarkerGui(StarSystem m, int mouseX, int mouseY)
	{
		if ((m != null) && (this.mw.starSystemManager.selectedSystem == m))
		{
			// right clicked previously selected marker.
			// edit the marker
			this.mc.displayGuiScreen(new MwGuiStarSystemDialog(this, this.mw.starSystemManager, m));

		}
	}
}
