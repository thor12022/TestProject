package alcubierre.jump.client.gui;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.client.map.StarSystemManager;
import alcubierre.jump.galaxy.StarSystem;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class MwGuiStarSystemDialog extends MwGuiTextDialog
{
	private final StarSystemManager starSystemManager;
	private StarSystem editingSystem;
	private String systemName = "";
	private GalaxyVector systemPos = GalaxyVector.ORIGIN;
	private int state = 0;

	public MwGuiStarSystemDialog(GuiScreen parentScreen, StarSystemManager starSystemManager, String markerName, String markerGroup, GalaxyVector pos)
	{
		super(parentScreen, I18n.format("mw.gui.mwguimarkerdialog.title.new") + ":", markerName, I18n.format("mw.gui.mwguimarkerdialog.error"));
		this.starSystemManager = starSystemManager;
		this.systemName = markerName;
		this.systemPos = pos;
		this.editingSystem = null;
	}

	public MwGuiStarSystemDialog(GuiScreen parentScreen, StarSystemManager starSystemManager, StarSystem editingSystem)
	{
		super(parentScreen, I18n.format("mw.gui.mwguimarkerdialog.title.edit") + ":", editingSystem.getName(), I18n.format("mw.gui.mwguimarkerdialog.error"));
		this.starSystemManager = starSystemManager;
		this.editingSystem = editingSystem;
		this.systemName = editingSystem.getName();
		this.systemPos = editingSystem.getPosition();
	}

	@Override
	public boolean submit()
	{
		boolean done = false;
		switch (this.state)
		{
		case 0:
			this.systemName = this.getInputAsString();
			if (this.inputValid)
			{
				this.title = I18n.format("mw.gui.mwguimarkerdialog.title.x") + ":";
				this.setText("" + this.systemPos.getX());
				this.error = I18n.format("mw.gui.mwguimarkerdialog.error.x");
				this.state++;
			}
			break;
		case 1:
			this.systemPos = new GalaxyVector(this.getInputAsInt(), (int)this.systemPos.getZ());
			if (this.inputValid)
			{
				this.title = I18n.format("mw.gui.mwguimarkerdialog.title.z") + ":";
				this.setText("" + this.systemPos.getZ());
				this.error = I18n.format("mw.gui.mwguimarkerdialog.error.z");
				this.state++;
			}
			break;
		}
		return done;
	}
}
