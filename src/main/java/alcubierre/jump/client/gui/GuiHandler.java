package alcubierre.jump.client.gui;

import alcubierre.jump.Jump;
import alcubierre.jump.machines.alloyer.ContainerAlloyer;
import alcubierre.jump.machines.alloyer.GUIAlloyer;
import alcubierre.jump.machines.alloyer.TileAlloyer;
import alcubierre.jump.machines.generator.ContainerGenerator;
import alcubierre.jump.machines.generator.GUIGenerator;
import alcubierre.jump.machines.generator.TileGenerator;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.Map;

public class GuiHandler implements IGuiHandler
{
   public static final int GENERATOR = 1;
   public static final int ALLOYER = 2;

   protected final Map<Integer, IGuiHandler> guiHandlers = new HashMap<>();

   public void registerGUIHandler(int ID, IGuiHandler handler)
   {
      guiHandlers.put(ID, handler);
   }

   @Override
   public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
   {
      BlockPos pos = new BlockPos(x, y, z);
      TileEntity te = world.getTileEntity(pos);
      if (te instanceof TileGenerator)
      {
         return new ContainerGenerator(player.inventory, (TileGenerator) te);
      }
      else if(te instanceof TileAlloyer)
      {
         return new ContainerAlloyer(player.inventory, (TileAlloyer) te);
      }
      return null;
   }

   @Override
   public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
   {
      BlockPos pos = new BlockPos(x, y, z);
      TileEntity te = world.getTileEntity(pos);
      if (te instanceof TileGenerator)
      {
         TileGenerator containerTileEntity = (TileGenerator) te;
         return new GUIGenerator(containerTileEntity, new ContainerGenerator(player.inventory, containerTileEntity));
      }
      else if(te instanceof TileAlloyer)
      {
         TileAlloyer containerTileEntity = (TileAlloyer) te;
         return new GUIAlloyer(containerTileEntity, new ContainerAlloyer(player.inventory, containerTileEntity));
      }
      return null;
   }
}
