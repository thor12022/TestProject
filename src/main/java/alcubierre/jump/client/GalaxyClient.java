package alcubierre.jump.client;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.galaxy.Galaxy;
import alcubierre.jump.galaxy.sector.Sector;
import alcubierre.jump.galaxy.sector.SectorState;
import alcubierre.jump.network.MessageGalaxyServerUpdate;

/**
 * @todo data sync with Server
 *
 */
public class GalaxyClient extends Galaxy
{
   public GalaxyClient()
   {
      super();
   }

   @Override
   public Sector getSector(GalaxyVector position)
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public void updateGalaxy()
   {
      
   }

   @Override
   public boolean isSectorLoaded(GalaxyVector position)
   {
      // TODO Auto-generated method stub
      return false;
   }
   
   public void handleServerUpdate(MessageGalaxyServerUpdate msg)
   {
      for(Sector chunk : msg.getUpdatedChunks())
      {
         loadedSectors.put(new GalaxyVector(chunk.getX(),  chunk.getZ()), new SectorState(chunk));
      }
   }
}
