package alcubierre.jump.galaxy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.IGalaxy;
import alcubierre.jump.api.IStarSystem;
import alcubierre.jump.galaxy.sector.Sector;
import alcubierre.jump.galaxy.sector.SectorState;
import net.minecraft.entity.player.EntityPlayer;

public abstract class Galaxy implements IGalaxy
{
   final protected Map<GalaxyVector, SectorState> loadedSectors = new HashMap<>();
   
   public GalaxyVector getPos(final EntityPlayer player)
   {
      return getPos(player.getEntityWorld().provider.getDimension());
   }
   
   // TODO: implement GalaxyVector from Dimension ID
   public GalaxyVector getPos(final int dimensionId)
   {
      return GalaxyVector.ORIGIN;
   }
   
   abstract public boolean isSectorLoaded(GalaxyVector position);
   
   /**
    * @param position This should the the Chunk's corner point
   **/
   abstract public Sector getSector(GalaxyVector position);
   
   abstract public void updateGalaxy();
   
   /* (non-Javadoc)
    * @see alcubierre.jump.galaxy.IGalaxy#getSystemsInBS(alcubierre.jump.api.GalaxyVector, double)
    */
   @Override
   @Nonnull
   public Collection<IStarSystem> getSystemsInBS(final GalaxyVector position, final double radius)
   {
      final Collection<IStarSystem> systems = new ArrayList<>();
      // Iterate over all the Sectors "within" the radius. It would be good to keep the 
      //    coordinates better encapsulated in the GalaxyVector
      //    It would also be good to avoid possible wasted checks in the corners
      final double minX = position.getX() - radius;
      final double minZ = position.getZ() - radius;
      final double maxX = position.getX() + radius;
      final double maxZ = position.getZ() + radius;
      for(double currX = minX; currX < maxX; currX += Sector.SIZE)
      {
         for(double currZ = minZ; currZ < maxZ; currZ += Sector.SIZE)
         {
            SectorState sector = loadedSectors.get(new GalaxyVector(currX, currZ));
            if(sector != null)
            {
               Collection<StarSystem> sectorSystems = sector.getSector().getSystemsInBS(position, radius);
               systems.addAll(sectorSystems);
            }
         }
      }
      return systems;
   }

   @Override
   public Collection<IStarSystem> getSystems()
   {
      final Collection<IStarSystem> systems = new ArrayList<>();
      loadedSectors.values().forEach(sect->systems.addAll(sect.getSector().getSystems()));
      return systems;
   }
   /**
    * @return an unmodifiable collection of the loaded sectors
    */
   public Collection<SectorState> getLoadedSectors()
   {
      return Collections.unmodifiableCollection(loadedSectors.values());
   }
}
