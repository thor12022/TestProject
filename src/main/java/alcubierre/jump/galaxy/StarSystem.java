package alcubierre.jump.galaxy;

import java.awt.Point;

import alcubierre.jump.Jump;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.IStarSystem;
import io.netty.buffer.ByteBuf;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.server.FMLServerHandler;

public class StarSystem implements IStarSystem
{
   private MutableGalaxyVector   position;
   private MutableGalaxyVector   velocity;
   private String                name;
   private int                   color;
   private int                   mass;

   // TODO: This should be better designed
   public Point.Double screenPos = new Point.Double(0, 0);
   
   public StarSystem(GalaxyVector position, GalaxyVector velocity)
   {
      this.position = MutableGalaxyVector.makeMutable(position);
      this.velocity = MutableGalaxyVector.makeMutable(velocity);
      // TODO: name the star systems
      name = (new Long(Jump.RAND.nextLong())).toString();
      // TODO Star system physical attributes - color, mass, brightness
      color = Jump.RAND.nextInt();
      mass = Jump.RAND.nextInt();
   }

   public StarSystem(ByteBuf buf)
   {
      position = new MutableGalaxyVector(buf);
      velocity = new MutableGalaxyVector(buf);
      name = ByteBufUtils.readUTF8String(buf);
      color = buf.readInt();
      mass = buf.readInt();
   }

   public void toBytes(ByteBuf buf)
   {
      position.toBytes(buf);
      velocity.toBytes(buf);
      ByteBufUtils.writeUTF8String(buf, name);
      buf.writeInt(color);
      buf.writeInt(mass);
   }

   @Override
   public GalaxyVector getPosition()
   {
      return position;
   }
   
   @Override
   public GalaxyVector getVelocity()
   {
      return position;
   }
   
   @Override
   public World getMcWorld()
   {
      // TODO Associate StarSystem with dimensions
      return FMLServerHandler.instance().getServer().getEntityWorld();
   }
   
   @Override
   public String getName()
   {
      return name;
   }
   
   @Override
   public int getColor()
   {
      return color;
   }
   
   @Override
   public int getMass()
   {
      // TODO Auto-generated method stub
      return mass;
   }
   
   public void setPosition(GalaxyVector position)
   {
      this.position.setTo(position);
   }
   
   public void setVelocity(GalaxyVector velocity)
   {
      this.velocity.setTo(velocity);
   }
}
