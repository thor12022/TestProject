package alcubierre.jump.galaxy.sector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nonnull;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.galaxy.StarSystem;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.ChunkPos;

public class Sector
{
   public static final int SIZE = 64;
   
   public static GalaxyVector getOrigin(GalaxyVector pointInChunk)
   {
      return new GalaxyVector(((int)pointInChunk.getX()) / SIZE, ((int)pointInChunk.getZ()) / SIZE);
   }

   private final int xOrigin;
   private final int zOrigin;

   private ArrayList<StarSystem> systems = new ArrayList<>();

   public Sector(int x, int z)
   {
      this.xOrigin = x;
      this.zOrigin = z;
   }
   
   public Sector(GalaxyVector pointInChunk)
   {
      this.xOrigin = ((int)pointInChunk.getX()) / SIZE;
      this.zOrigin = ((int)pointInChunk.getZ()) / SIZE;
   }
   
   public Sector(ByteBuf buf)
   {
      xOrigin = buf.readInt();
      zOrigin = buf.readInt();
      final int sysSize = buf.readInt();
      for(int i = 0; i < sysSize; ++i)
      {
         systems.add(new StarSystem(buf));
      }
   }

   @Override
   public String toString()
   {
      return String.format("(%d, %d)", this.xOrigin, this.zOrigin);
   }

   @Override
   public int hashCode()
   {
      return (int)ChunkPos.chunkXZ2Int(this.xOrigin, this.zOrigin);
   }

   @Override
   public boolean equals(Object obj)
   {
      if(obj == null || this == obj)
      {
         return true;
      }

      if(getClass() != obj.getClass())
      {
         return false;
      }

      Sector other = (Sector)obj;
      if(xOrigin != other.xOrigin || zOrigin != other.zOrigin)
      {
         return false;
      }

      return true;
   }

   public void toBytes(ByteBuf buf)
   {
      buf.writeInt(xOrigin);
      buf.writeInt(zOrigin);
      buf.writeInt(systems.size());
      for(StarSystem system : systems)
      {
         system.toBytes(buf);
      }
   }

   public int getX()
   {
      return xOrigin;
   }

   public int getZ()
   {
      return zOrigin;
   }

   public void addStarSytem(StarSystem system)
   {
      systems.add(system);
   }
   
   /**
    * @return a collection of Star Systems in the Bounding Sphere
   **/
   @Nonnull
   public Collection<StarSystem> getSystemsInBS( final GalaxyVector pos, final double radius)
   {
      final ArrayList<StarSystem> returnList = new ArrayList<>();
      final double radiusSq = radius * radius;
      for(StarSystem system : systems)
      {
         if(system.getPosition().distanceSq(pos) < radiusSq)
         {
            returnList.add(system);
         }
      }
      return returnList;
   }
   
   /**
    * @return an unmodifiable collection of Star Systems
   **/
   @Nonnull
   public Collection<StarSystem> getSystems()
   {
      return Collections.unmodifiableCollection(systems);
   }
   
   

   // changed to use the NBTTagCompound that minecraft uses. this makes the
   // local way of saving anvill data the same as Minecraft world data
   private NBTTagCompound writeChunkToNBT()
   {
      NBTTagCompound level = new NBTTagCompound();
      NBTTagCompound compound = new NBTTagCompound();
      level.setTag("Level", compound);

      compound.setInteger("xPos", this.xOrigin);
      compound.setInteger("zPos", this.zOrigin);
      return level;
   }

}
