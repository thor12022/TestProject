package alcubierre.jump.galaxy.sector;

import alcubierre.jump.config.Configurable;
import alcubierre.jump.galaxy.StarSystem;

@Configurable(sectionName="galaxy")
public class SectorState
{
   final private Sector sector;
   
   private int ticksLoaded = 0;
   
   /** the number a ticks a player has not loaded this sector **/
   private int ticksAbandoned = -1;
   
   /** if this sector will stay loaded even if abandoned **/
   private boolean forcedLoaded = false;
   
   public SectorState(Sector chunk)
   {
      this.sector = chunk;
   }

   public Sector getSector()
   {
      return sector;
   }
   
   public void tick()
   {
      ++ticksLoaded;
      
      
      // Check to see if the Sector has been abandoned
      if(ticksAbandoned < 0)
      {
         boolean containsPlayer = false;
         
         for(StarSystem system : sector.getSystems())
         {
            if(!system.getMcWorld().playerEntities.isEmpty())
            {
               containsPlayer = true;
               break;
            }
         }
         if(!containsPlayer)
         {
            ticksAbandoned = 0;
         }
      }
      else
      {
         ++ticksAbandoned;
      }
   }

   public int getTicksLoaded()
   {
      return ticksLoaded;
   }

   public int getTicksAbandoned()
   {
      return ticksAbandoned;
   }
   
   /**
    * 
    * @param isAbandoned true if the last player stopped loading sector, false if first player started loading sector
    * @note if sector abandoned state is identical to what it is getting set to, this does nothing
    */
   public void setAbandoned(boolean isAbandoned)
   {
      if(isAbandoned && this.ticksAbandoned < 0)
      {
         this.ticksAbandoned = 0;
      }
      else if(!isAbandoned)
      {
         this.ticksAbandoned = -1;
      }
   }
   
   public boolean isForcedLoaded()
   {
      return forcedLoaded;
   }

   public void setForcedLoaded(boolean forcedLoaded)
   {
      this.forcedLoaded = forcedLoaded;
   }

   public boolean isDirty()
   {
      // TODO Auto-generated method stub
      return true;
   }
}
