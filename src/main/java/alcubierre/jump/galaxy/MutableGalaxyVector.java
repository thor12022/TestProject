package alcubierre.jump.galaxy;

import alcubierre.jump.api.GalaxyVector;
import io.netty.buffer.ByteBuf;

public class MutableGalaxyVector extends GalaxyVector
{

   static MutableGalaxyVector makeMutable(GalaxyVector pos)
   {
      if(pos instanceof MutableGalaxyVector)
      {
         return (MutableGalaxyVector)pos;
      }
      return new MutableGalaxyVector(pos.getX(), pos.getZ());
   }
   
   public MutableGalaxyVector(double x, double z)
   {
      super(x, z);
   }
   
   public MutableGalaxyVector(ByteBuf buf)
   {
      super(buf);
   }
   
   public void setTo(GalaxyVector pos)
   {
      x = pos.getX();
      z = pos.getZ();
   }
   
   public void addTo(double x, double y)
   {
      this.x += x;
      this.z += z;
   }
}
