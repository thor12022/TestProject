package alcubierre.jump.util;

import java.util.HashSet;
import java.util.regex.Pattern;

import com.google.common.collect.Sets;

import net.minecraft.util.ResourceLocation;

public final class Reference
{
	public static final String VersionURL = "";
	public static final String ForgeVersionURL = "";

	public static final String catOptions = "options";
	public static final String catLargeMapConfig = "largemap";
	public static final String catSmallMapConfig = "smallmap";
	public static final String catFullMapConfig = "fullscreenmap";
	public static final String catMapPos = "mappos";

	public static final String PlayerTrailName = "player";

	public static final Pattern patternInvalidChars = Pattern.compile("[^\\p{L}\\p{Nd}_]");
	public static final Pattern patternInvalidChars2 = Pattern.compile("[^\\p{L}\\p{Nd}_ -]");

	public static final String catWorld = "world";
	public static final String catMarkers = "markers";
	public static final String worldDirConfigName = "jump.cfg";

	public static final ResourceLocation backgroundTexture = new ResourceLocation(
			"jump",
			"textures/map/background.png");
	public static final ResourceLocation roundMapTexture = new ResourceLocation(
			"jump",
			"textures/map/border_round.png");
	public static final ResourceLocation squareMapTexture = new ResourceLocation(
			"jump",
			"textures/map/border_square.png");
	public static final ResourceLocation playerArrowTexture = new ResourceLocation(
			"jump",
			"textures/map/arrow_player.png");
	public static final ResourceLocation northArrowTexture = new ResourceLocation(
			"jump",
			"textures/map/arrow_north.png");
	public static final ResourceLocation leftArrowTexture = new ResourceLocation(
			"jump",
			"textures/map/arrow_text_left.png");
	public static final ResourceLocation rightArrowTexture = new ResourceLocation(
			"jump",
			"textures/map/arrow_text_right.png");
	public static final ResourceLocation DummyMapTexture = new ResourceLocation(
			"jump",
			"textures/map/dummy_map.png");

	public static final HashSet<String> PROTOCOLS = Sets.newHashSet(new String[]
	{
			"http",
			"https"
	});
}
