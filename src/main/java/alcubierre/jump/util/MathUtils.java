package alcubierre.jump.util;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

public class MathUtils
{
   /**
    * 
    * @param num
    * @return the pair of factors closest to the square root
    * @note not actually proven to work, might miss sometimes
    */
   @Nullable
   public static Pair<Integer, Integer> getClosestFactors(final int num)
   {
      double value1 = Math.sqrt(num);
      double value2 = value1;
      double roundValue1 = Math.floor(value1);
      double roundValue2 = Math.ceil(value1);
      boolean bRoundDir = false;
      while(roundValue1  != 0 && roundValue2 != 0)
      {
         value1 = num / roundValue1;
         if(value1 == Math.floor(value1))
         {
            return Pair.of((int)value1, (int)(num / value1));
         }
         roundValue1 = bRoundDir ? Math.ceil(value1) : Math.floor(value1);
         
         value2 = num / roundValue2;

         if(value1 == Math.floor(value1))
         {
            return Pair.of((int)value1, (int)(num / value1));
         }
         roundValue2 = bRoundDir ? Math.floor(value2) : Math.ceil(value2);
         bRoundDir = !bRoundDir;
      }
      return null;
   }
}
