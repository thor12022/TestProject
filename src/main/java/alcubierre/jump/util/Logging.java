package alcubierre.jump.util;

import alcubierre.jump.Jump;

@Deprecated
public class Logging
{
	public static void logInfo(String s, Object... args)
	{
		Jump.LOGGER.info(String.format(s, args));
	}

	public static void logWarning(String s, Object... args)
	{
		Jump.LOGGER.warn(String.format(s, args));
	}

	public static void logError(String s, Object... args)
	{
		Jump.LOGGER.error(String.format(s, args));
	}

	public static void debug(String s, Object... args)
	{
		Jump.LOGGER.debug(String.format(s, args));
	}

	public static void log(String s, Object... args)
	{
		logInfo(String.format(s, args));
	}
}
