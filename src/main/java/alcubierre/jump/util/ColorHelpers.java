package alcubierre.jump.util;

public class ColorHelpers
{
   /**
    * 
    * @param r Red, 0-255, only the 8 LSBs used
    * @param g Green, 0-255, only the 8 LSBs used
    * @param b Blue, 0-255, only the 8 LSBs used
    * @return integer value of color with assumed alpha of 0x00
    */
   public static int fromRgb(final int r, final int g, final int b)
   {
      return ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
   }

   /**
    * 
    * @param r Red, 0-255, only the 8 LSBs used
    * @param g Green, 0-255, only the 8 LSBs used
    * @param b Blue, 0-255, only the 8 LSBs used
    * @param b Alpha, 0-255, only the 8 LSBs used
    * @return integer value of color
    */
   public static int fromRgba(final int r, final int g, final int b, final int a)
   {
      return ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff) | ((a &0xff) << 24);
   }

   /**
    * @return percentage of Red in supplied colour
    */
   public static float getRed(final int colour)
   {
      return (((colour >> 16) & 0xff) / 255.0f);
   }

   /**
    * @return percentage of Green in supplied colour
    */
   public static float getGreen(final int colour)
   {
      return (((colour >> 8) & 0xff) / 255.0f);
   }

   /**
    * @return percentage of Blue in supplied colour
    */
   public static float getBlue(final int colour)
   {
      return (((colour) & 0xff) / 255.0f);
   }
}
