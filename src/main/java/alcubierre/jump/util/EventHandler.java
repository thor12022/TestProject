package alcubierre.jump.util;

import alcubierre.jump.ModInformation;
import alcubierre.jump.Jump;

/*
 * Class for most of your events to be registered in.
 * Remember that there are two different registries for Events. This one will not work for everything.
 */

import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandler {

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
        if (eventArgs.getModID().equals(ModInformation.ID)) {
            //ConfigHandler.syncConfig();
            Jump.LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.config.refresh"));
        }
    }
}
