package alcubierre.jump.api;

import java.util.Collection;

import javax.annotation.Nonnull;

public interface IGalaxy
{
   /**
    * Gets all the Star Systems within the Bounding Sphere provided by the given position and radius.
    * Will only return systems in loaded Sectors
    */
   @Nonnull
   Collection<IStarSystem> getSystemsInBS(GalaxyVector position, double radius);
   
   /**
    * Gets all the Star Systems 
    */
   Collection<IStarSystem> getSystems();

}