package alcubierre.jump.api;

import javax.annotation.concurrent.Immutable;

import com.google.common.base.Objects;

import alcubierre.jump.network.IMessageComponent;
import io.netty.buffer.ByteBuf;

@Immutable
public class GalaxyVector implements Comparable<GalaxyVector>
{

   public static final GalaxyVector ORIGIN = new GalaxyVector(0, 0);
   
   protected double x;
   protected double z; 
   
   public GalaxyVector(double x, double z)
   {
      this.x = x;
      this.z = z;
   }
   
   public GalaxyVector(ByteBuf buf)
   {
      x = buf.readDouble();
      z = buf.readDouble();
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = prime * ((Double) x).hashCode();
      result *= prime * ((Double) z).hashCode();
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if(this == obj)
      {
         return true;
      }
      
      if(!(obj instanceof GalaxyVector))
      {
         return false;
      }
      
      GalaxyVector other = (GalaxyVector)obj;
      if(x != other.x || z != other.z)
      {
         return false;
      }
      
      return true;
   }

   @Override
   public int compareTo(final GalaxyVector o)
   {
      if (this.z < o.z)
      {
         return -1;
      }
      if (this.z > o.z)
      {
         return 1;
      }
      if (this.x < o.x)
      {
         return -1;
      }
      if (this.x > o.x)
      {
         return 1;
      }
      return 0;
   }

   @Override
   public String toString()
   {
       return Objects.toStringHelper(this).add("x", this.x).add("z", this.z).toString();
   }

   public void toBytes(ByteBuf buf)
   {
      buf.writeDouble(x);
      buf.writeDouble(z);
   }
   
   public double getX()
   {
      return x;
   }

   
   public double getZ()
   {
      return z;
   }

   public double atan2()
   {
      return Math.atan2(x, z);
   }
   
   public double distance(final double xIn,final double zIn)
   {
       return Math.sqrt(distanceSq(xIn, zIn));
   }
   
   public double distance(final GalaxyVector o)
   {
       return distance(o.getX(), o.getZ());
   }

   public double distanceSq(final GalaxyVector o)
   {
      return distanceSq(o.getX(), o.getZ());
   }

   public GalaxyVector add(final GalaxyVector o)
   {
      return new GalaxyVector(this.x + o.getX(), this.z + o.getZ());
   }
   
   public double distanceSq(final double xIn,final double zIn)
   {
       double d0 = this.x - xIn;
       double d1 = this.z - zIn;
       return d0 * d0 + d1 * d1;
   }

   public GalaxyVector sub(final GalaxyVector o)
   {
      return new GalaxyVector(this.x - o.getX(), this.z - o.getZ());
   }
   
   public GalaxyVector add(final double xPos, final double zPos)
   {
      return new GalaxyVector(this.x + xPos, this.z + zPos);
   }
   
   public GalaxyVector add(final int xPos, final int zPos)
   {
      return new GalaxyVector(this.x + xPos, this.z + zPos);
   }

   public GalaxyVector sub(final double xPos, final double zPos)
   {
      return new GalaxyVector(this.x - xPos, this.z - zPos);
   }
   
   public GalaxyVector sub(final int xPos, final int zPos)
   {
      return new GalaxyVector(this.x - xPos, this.z - zPos);
   }
   
   public GalaxyVector mult(final double val)
   {
      return new GalaxyVector(this.x * val, this.z * val);
   }
   
   public GalaxyVector sub(final int val)
   {
      return new GalaxyVector(this.x * val, this.z * val);
   }
   
   public GalaxyVector div(final double val)
   {
      return new GalaxyVector(this.x / val, this.z / val);
   }
   
   public GalaxyVector div(final int val)
   {
      return new GalaxyVector(this.x / val, this.z / val);
   }
}
