package alcubierre.jump.api;

/**
 * Thrown if the API is used improperly, e.g. called before being initialized
 *
 */
public class InvalidApiUsageException extends JumpException
{
  private static final long serialVersionUID = 1L;

   public InvalidApiUsageException(String string)
   {
      super("Invalid API Usage: " + string);
   }

}
