package alcubierre.jump.api;

import javax.annotation.Nullable;

import net.minecraft.entity.player.EntityPlayer;

/**
 * This should not be implemented
 */
public interface IJumpApi
{
   /**
    * @return the IGalaxy object for the Client, will be null if dedicated server
    */
   @Nullable
   IGalaxy getClientGalaxy();
   
   /**
    * @return the IGalaxy object for the Server, will be null if remote world
    */
   @Nullable
   IGalaxy getServerGalaxy();
   
   /**
    * The player's centerPos in the galaxy.
    * Namely the coordinates of the dimension they are in.
    */
   GalaxyVector getPlayerGalaxyPos(final EntityPlayer player);
}
