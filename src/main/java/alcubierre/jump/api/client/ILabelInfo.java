package alcubierre.jump.api.client;

public interface ILabelInfo
{
	// StringArray of text to render in the hover label, each centerPos in the
	// array is a newline
	public String[] getInfoText();
}
