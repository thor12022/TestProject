package alcubierre.jump.api.client;

import java.util.ArrayList;

import alcubierre.jump.api.GalaxyVector;

public interface IMwDataProvider
{
	public ArrayList<IMwChunkOverlay> getChunksOverlay(GalaxyVector centerPos,
			double minX, double minZ, double maxX, double maxZ);

	// Returns what should be added to the status bar by the addon.
	public String getStatusString(GalaxyVector pos);

	// Call back for middle click.
	public void onMiddleClick(GalaxyVector pos, IMapView mapview);

	public void onMapCenterChanged(GalaxyVector vPos, IMapView mapview);

	public void onZoomChanged(int level, IMapView mapview);

	public void onOverlayActivated(IMapView mapview);

	public void onOverlayDeactivated(IMapView mapview);

	public void onDraw(IMapView mapview, IMapMode mapmode);

	public boolean onMouseInput(IMapView mapview, IMapMode mapmode);

	// return null if nothing should be drawn on fullscreen map
	public ILabelInfo getLabelInfo(int mouseX, int mouseY);
}
