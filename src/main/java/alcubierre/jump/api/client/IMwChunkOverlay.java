package alcubierre.jump.api.client;

import alcubierre.jump.api.GalaxyVector;

public interface IMwChunkOverlay
{
	public GalaxyVector getCoordinates();

	public int getColor();

	public float getFilling();

	public boolean hasBorder();

	public float getBorderWidth();

	public int getBorderColor();
}
