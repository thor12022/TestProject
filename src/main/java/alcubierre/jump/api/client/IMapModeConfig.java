package alcubierre.jump.api.client;

public interface IMapModeConfig
{

	public String[] getCoordsModeStringArray();

	public boolean getEnabled();

	public boolean getCircular();

	public String getCoordsMode();

	public boolean getBorderMode();

	public int getPlayerArrowSize();

	public int getMarkerSize();

	public int getAlphaPercent();

	public String getBiomeMode();

	public double getXPos();

	public double getYPos();

	public double getHeightPercent();

	public double getWidthPercent();
}
