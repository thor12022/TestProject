package alcubierre.jump.api.client;

import alcubierre.jump.api.GalaxyVector;

public interface IMapView
{
	public void setViewCentre(GalaxyVector position);

	/**
    * Use {@link #getCenter()}:{@link GalaxyVector}
   **/
	@Deprecated
   public double getX();

	
	/**
	 * Use {@link #getCenter()}:{@link GalaxyVector}
	 */
   @Deprecated
	public double getZ();
	
	public GalaxyVector getCenter();

	public double getWidth();

	public double getHeight();

	public void panView(double relX, double relZ);

	public int setZoomLevel(int zoomLevel);

	public int adjustZoomLevel(int n);

	public int getZoomLevel();

	public int getRegionZoomLevel();

	// bX and bZ are the coordinates of the block the zoom is centred on.
	// The relative centerPos of the block in the view will remain the same
	// as before the zoom.
	public void zoomToPoint(int newZoomLevel, GalaxyVector position);

	public void setMapWH(int w, int h);

	public void setMapWH(IMapMode mapMode);

	public double getMinX();

	public double getMaxX();

	public double getMinZ();

	public double getMaxZ();

	public void setViewCentreScaled(GalaxyVector position);

	public void setTextureSize(int n);

	public int getPixelsPerBlock();

	public boolean isBlockWithinView(GalaxyVector position, boolean circular);
}
