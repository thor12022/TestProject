package alcubierre.jump.api;

import net.minecraft.world.World;

public interface IStarSystem
{

   GalaxyVector getPosition();

   GalaxyVector getVelocity();

   World getMcWorld();
   
   String getName();

   int getColor();
   
   int getMass();

}