package alcubierre.jump.api;

public class JumpException extends Exception
{
   private static final long serialVersionUID = 1L;
   
   public JumpException(String string)
   {
      super(string);
   }
}
