package alcubierre.jump.api;

public class Jump
{ 
   static IJumpApi instance;
   
   public static IJumpApi getApi() throws InvalidApiUsageException
   {
      if(instance != null)
      {
         return instance;
      }
      throw new InvalidApiUsageException("The Jump API has not been initialized yet");
   }
}
