package alcubierre.jump.server.galaxy;

import java.util.Random;

import org.apache.commons.lang3.tuple.Pair;

import alcubierre.jump.Jump;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.config.Config;
import alcubierre.jump.config.Configurable;
import alcubierre.jump.galaxy.StarSystem;
import alcubierre.jump.galaxy.sector.Sector;
import alcubierre.jump.util.MathUtils;

@Configurable
public class SectorGenerator extends AbstractSectorGenerator
{
   @Config(minInt = 1, maxInt = Sector.SIZE, comment="A perfect square would be best, a prime is worst")
   private int systemChancesPerChunk = 4;
   
   @Config(minFloat = 0, maxFloat  = 1)
   private float systemChance = 0.3f;

   final private Random rand = new Random(galaxy.getSeed());
   
   SectorGenerator(GalaxyServer galaxy)
   {
      super(galaxy);
      
      Jump.CONFIG.register(this);
      
      if(systemChancesPerChunk > 1)
      {
         systemChancesPerChunk &= 0xE;
         Jump.CONFIG.syncConfig(this);
      }
   }

   @Override
   public Sector Generate(GalaxyVector pos)
   {
      // setup the seed for this chunk
      final long k = this.rand.nextLong() / 2L * 2L + 1L;
      final long l = this.rand.nextLong() / 2L * 2L + 1L;
      rand.setSeed((long)pos.getX() * k + (long)pos.getZ() * l ^ this.galaxy.getSeed());
      
      Sector chunk = new Sector(pos);
      
      Pair<Integer, Integer> size = MathUtils.getClosestFactors(systemChancesPerChunk);
      for(int x = chunk.getX(); x < size.getLeft() + Sector.SIZE; x += size.getLeft())
      {
         for(int z = chunk.getZ(); z < size.getRight() + Sector.SIZE; z += size.getRight())
         {
            if(rand.nextFloat() < systemChance)
            {
               final double xs =  (x + (Sector.SIZE / 2 ) ) * rand.nextGaussian();
               final double zs = (z + (Sector.SIZE / 2 ) ) * rand.nextGaussian();
               //! @todo add StarSystem initial velocity
               StarSystem system = new StarSystem(new GalaxyVector(xs, zs), new GalaxyVector(xs, zs));
               chunk.addStarSytem(system);
            }
         }
      }
      return chunk;
   }

}
