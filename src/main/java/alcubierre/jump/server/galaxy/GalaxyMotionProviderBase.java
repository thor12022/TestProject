package alcubierre.jump.server.galaxy;

public abstract class GalaxyMotionProviderBase
{
   abstract void onTick(GalaxyServer galaxy);
   
   abstract String name();
}
