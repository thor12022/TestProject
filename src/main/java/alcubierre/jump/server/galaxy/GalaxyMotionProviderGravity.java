package alcubierre.jump.server.galaxy;

import alcubierre.jump.Jump;
import alcubierre.jump.config.Config;
import alcubierre.jump.config.Configurable;

@Configurable
public class GalaxyMotionProviderGravity extends GalaxyMotionProviderBase
{
   private final static String NAME = "gravity";
   
   @Config(comment = "How fast time passes in the galaxy in regards to Star System Motion. Making it too high will probably get wierd",
           minFloat=0f)
   private static float timeMultiplier = 1;
 
   @Config(comment = "How big a range of chunks are taken into account for gravity. 0 for only the single chunk, 1 for adjacent chunks",
           minInt = 0)
   private static int galaxyChunkRange = 1;
   
   static
   {
      Jump.CONFIG.register(GalaxyMotionProviderGravity.class);
   }
   
   @Override
   void onTick(GalaxyServer galaxy)
   {
      // iterate over chunks in phases
      // Phase 1: Sum up the accel due to gravity on each system, use galaxyChunkRange to determine which systems get used
      // Phase 2: Apply the velocity change to all systems
   }

   @Override
   String name()
   {
      return NAME;
   }
}