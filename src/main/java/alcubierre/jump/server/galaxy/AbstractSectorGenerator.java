package alcubierre.jump.server.galaxy;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.galaxy.sector.Sector;

abstract class AbstractSectorGenerator
{
   final protected GalaxyServer galaxy;
   
   AbstractSectorGenerator(GalaxyServer galaxy)
   {
      this.galaxy = galaxy;
   }
   
   abstract Sector Generate(GalaxyVector pos);
}
