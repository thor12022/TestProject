package alcubierre.jump.server.galaxy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alcubierre.jump.Jump;
import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.config.Config;
import alcubierre.jump.config.Configurable;
import alcubierre.jump.galaxy.Galaxy;
import alcubierre.jump.galaxy.sector.Sector;
import alcubierre.jump.galaxy.sector.SectorState;
import alcubierre.jump.network.MessageGalaxyClientStatus;
import alcubierre.jump.network.MessageGalaxyServerUpdate;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

@Configurable
public class GalaxyServer extends Galaxy
{
   private final static Map<String, GalaxyMotionProviderBase> MOTION_REGISTRY = new HashMap<>();
   
   
   @Config(comment = "The name of algorithm for governing galaxy motion: ['', 'gravity']")
   private static String motionProviderName = "gravity";
   
   @Config(comment = "Nuber of ticks to wait between syncing")
   private static int syncPeriod = 20;
   
   @Config(comment = "The number of ticks waited before a client is assumed to no longer be interested")
   private static int playerTimeoutTicks = 40;
   
   @Config(comment = "The radius of sectors around a player to load", minInt = 0)
   private static int sectorPlayerLoadRange = 2;
   
   private long seed = Jump.RAND.nextLong();
   
   private GalaxyMotionProviderBase motionProvider = null;
   
   private long tick = 0;
   
   static 
   {
      Jump.CONFIG.register(GalaxyServer.class);
      GalaxyMotionProviderBase provider = new GalaxyMotionProviderGravity();
      MOTION_REGISTRY.put(provider.name(), provider);
   }
   
   final private Map<EntityPlayerMP, Long> viewingPlayers = new HashMap<>();
   
   AbstractSectorGenerator sectorGenerator = new SectorGenerator(this);
   
   public GalaxyServer()
   {
      super();
      
      if(motionProviderName != "")
      {
         motionProvider = MOTION_REGISTRY.get(motionProviderName);
         
         if(motionProvider == null)
         {
            Jump.LOGGER.warn("GalaxyServer cannot find Motion Provider: " + motionProviderName + " Galaxy will be static");
            motionProviderName = "";
            Jump.CONFIG.syncConfig(GalaxyServer.class);
         }
      }
      
      
      MinecraftForge.EVENT_BUS.register(this);
   }
   
   public void readFromNBT(NBTTagCompound nbt)
   {
      // TODO Auto-generated method stub
      
   }

   public NBTTagCompound writeToNBT(NBTTagCompound compound)
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public boolean isSectorLoaded(GalaxyVector position)
   {
      GalaxyVector sectorPos = Sector.getOrigin(position);
      return loadedSectors.containsKey(sectorPos);
   }
   
   /**
    * @note this will load the sector as abandoned (see {@link SectorState}) if it is not already loaded
   **/
   @Override
   public Sector getSector(GalaxyVector position)
   {
      SectorState sectorState = loadedSectors.get(Sector.getOrigin(position));
      if(sectorState == null)
      {
         sectorState = new SectorState(loadSector(position));
      }
      return sectorState.getSector();
   }
   

   @Override
   public void updateGalaxy()
   {
      final List<Sector> dirtySectors = new ArrayList<>();
      
      loadedSectors.values().forEach(sectorState -> 
         {
            if(sectorState.isDirty())
            {
               dirtySectors.add(sectorState.getSector());
            }
         });
      
      MessageGalaxyServerUpdate updateMsg = new MessageGalaxyServerUpdate(dirtySectors);
      
      viewingPlayers.keySet().forEach(player -> Jump.NETWORK.sendTo(updateMsg, player));
   }

   public long getSeed()
   {
      return seed;
   }
   
   public void init()
   {}
   
   @SubscribeEvent
   public void onServerTickEvent(TickEvent.ServerTickEvent event)
   {
      if(event.phase == TickEvent.Phase.START)
      {
         ++tick;
         
         viewingPlayers.values().removeIf(timeoutTick -> tick >= timeoutTick);
         
         if(motionProvider != null)
         {
            motionProvider.onTick(this);
         }
         
         if(tick % syncPeriod == 0)
         {
            updateGalaxy();
         }
      }
   }
   
   @SubscribeEvent
   public void onEntityJoinWorld(EntityJoinWorldEvent event)
   {
      if(event.getEntity() instanceof EntityPlayerMP)
      {
         loadSector(getPos((EntityPlayerMP)event.getEntity()));
      }
   }

   public void handleClientStatus(final MessageGalaxyClientStatus msg, final EntityPlayerMP player)
   {
      switch(msg.getMsgType())
      {
         case SHOW:
         {
            viewingPlayers.put(player, tick + playerTimeoutTicks);
            break;
         }
         case HIDE:
         {
            viewingPlayers.remove(player);
            break;
         }
         default:
         {
            break;
         }
      }
   }
   
   private Sector loadSector(GalaxyVector position)
   {
      GalaxyVector sectorPos = Sector.getOrigin(position);
      // TODO Try read sector from disk save
      Sector sector = null;
      
      if(sector == null)
      {
         sector = sectorGenerator.Generate(sectorPos);
      }
      loadedSectors.put(sectorPos, new SectorState(sector));
      return sector;
   }
}
