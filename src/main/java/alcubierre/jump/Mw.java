package alcubierre.jump;

import java.io.File;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import alcubierre.jump.api.GalaxyVector;
import alcubierre.jump.api.IGalaxy;
import alcubierre.jump.api.IJumpApi;
import alcubierre.jump.client.GalaxyClient;
import alcubierre.jump.client.MwKeyHandler;
import alcubierre.jump.client.gui.MwGui;
import alcubierre.jump.client.map.StarSystemManager;
import alcubierre.jump.config.ConfigData;
import alcubierre.jump.galaxy.StarSystem;
import alcubierre.jump.network.MessageGalaxyClientStatus;
import alcubierre.jump.server.galaxy.GalaxyServer;
import alcubierre.jump.util.Logging;
import alcubierre.jump.util.Render;
import alcubierre.jump.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.DimensionManager;

public class Mw implements IJumpApi
{
	public Minecraft mc = null;

	GalaxyClient galaxyClient = null;
   GalaxyServer galaxyServer = null;
	
	// directories
	private final File saveDir;
	public File worldDir = null;
	public File imageDir = null;

	// flags and counters
	public boolean ready = false;
	// public boolean multiplayer = false;
	public int tickCounter = 0;

	public int textureSize = 2048;

	public int playerDimension = 0;

	// instances of components
	public BackgroundExecutor executor = null;
	public StarSystemManager starSystemManager = null;

	Mw()
	{
		// client only initialization
		this.mc = Minecraft.getMinecraft();

		// create base save directory
		this.saveDir = new File(this.mc.mcDataDir, "saves");

		this.ready = false;
	}

   @Override
   public GalaxyVector getPlayerGalaxyPos(EntityPlayer player)
   {
      return !player.worldObj.isRemote ? galaxyServer.getPos(player.dimension) : galaxyClient.getPos(player.dimension);
   }
   
   @Nonnull
   public IGalaxy initServer()
   {
      galaxyServer = new GalaxyServer();
      return galaxyServer;
   }

   @Nonnull
   public IGalaxy initClient()
   {
      galaxyClient = new GalaxyClient();
      return galaxyClient;
   }

   @Override
   @Nullable
   public GalaxyServer getServerGalaxy()
   {
      return galaxyServer;
   }
   
   @Override
   @Nullable
   public GalaxyClient getClientGalaxy()
   {
      return galaxyClient;
   }

	public void setTextureSize()
	{
		if (ConfigData.configTextureSize != this.textureSize)
		{
			int maxTextureSize = Render.getMaxTextureSize();
			int textureSize = 1024;
			while ((textureSize <= maxTextureSize) && (textureSize <= ConfigData.configTextureSize))
			{
				textureSize *= 2;
			}
			textureSize /= 2;

			Logging.log("GL reported max texture size = %d", maxTextureSize);
			Logging.log("texture size from config = %d", ConfigData.configTextureSize);
			Logging.log("setting map texture size to = %d", textureSize);

			this.textureSize = textureSize;
		}
	}
	
	public void teleportToSystem(StarSystem system)
	{
	   if (ConfigData.teleportEnabled)
      {
		   Utils.printBoth("Teleporting to systems currently unimplemented");
		}
	}

	
	// //////////////////////////////
	// Initialization and Cleanup
	// //////////////////////////////

	public void load()
	{
		if (this.ready)
		{
			return;
		}

		if ((this.mc.theWorld == null) || (this.mc.thePlayer == null))
		{
			Logging.log("Mw.load: world or player is null, cannot load yet");
			return;
		}

		Logging.log("Mw.load: loading...");

		// get world and image directories
		File saveDir = this.saveDir;
		if (ConfigData.saveDirOverride.length() > 0)
		{
			File d = new File(ConfigData.saveDirOverride);
			if (d.isDirectory())
			{
		      saveDir = d;
			}
			else
			{
				Logging.log("error: no such directory %s", ConfigData.saveDirOverride);
			}
		}

		if (!this.mc.isSingleplayer())
		{

			this.worldDir = new File(
					new File(saveDir, "mapwriter_mp_worlds"),
					Utils.getWorldName());
		}
		else
		{
			saveDir = DimensionManager.getCurrentSaveRootDirectory();
			this.worldDir = new File(saveDir, "mapwriter");
		}

		// create directories
		this.imageDir = new File(this.worldDir, "images");
		if (!this.imageDir.exists())
		{
			this.imageDir.mkdirs();
		}
		if (!this.imageDir.isDirectory())
		{
			Logging.log(
					"Mapwriter: ERROR: could not create images directory '%s'",
					this.imageDir.getPath());
		}

		this.tickCounter = 0;

		// this.multiplayer = !this.mc.isIntegratedServerRunning();

		// marker manager only depends on the config being loaded
		this.starSystemManager = new StarSystemManager();

		// executor does not depend on anything
		this.executor = new BackgroundExecutor();

		this.ready = true;

		// if (!zoomLevelsExist) {
		// printBoth("recreating zoom levels");
		// this.regionManager.recreateAllZoomLevels();
		// }
	}

	// //////////////////////////////
	// Event handlers
	// //////////////////////////////

	public void onTick()
	{
		this.load();
		if (this.ready && (this.mc.thePlayer != null))
		{
			this.setTextureSize();

			// process background tasks
			int maxTasks = 50;
			while (!this.executor.processTaskQueue() && (maxTasks > 0))
			{
				maxTasks--;
			}

			// let the renderEngine know we have changed the bound texture.
			// this.mc.renderEngine.resetBoundTexture();

			// if (this.tickCounter % 100 == 0) {
			// MwUtil.log("tick %d", this.tickCounter);
			// }

			this.tickCounter++;
		}
	}

	public void onKeyDown(KeyBinding kb)
	{
		// make sure not in GUI element (e.g. chat box)
		if ((this.mc.currentScreen == null) && (this.ready))
		{
			// Mw.log("client tick: %s key pressed", kb.keyDescription);

			if (kb == MwKeyHandler.keyMapGui)
			{
				// open map gui
				this.mc.displayGuiScreen(new MwGui(this));
				Jump.NETWORK.sendToServer(new MessageGalaxyClientStatus(MessageGalaxyClientStatus.Type.SHOW));
			}
		}
	}
}
