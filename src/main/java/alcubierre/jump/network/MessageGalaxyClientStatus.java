package alcubierre.jump.network;

import alcubierre.jump.Jump;
import alcubierre.jump.server.galaxy.GalaxyServer;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageGalaxyClientStatus implements IMessage
{
   public static enum Type
   {
      SHOW,
      UPDATE,
      HIDE
   }

   public static class Handler implements IMessageHandler<MessageGalaxyClientStatus, IMessage>
   {
       @Override
       public IMessage onMessage(final MessageGalaxyClientStatus message, MessageContext ctx)
       {
           final EntityPlayerMP player = ctx.getServerHandler().playerEntity;
           final WorldServer world = (WorldServer) player.worldObj;

           world.addScheduledTask(new Runnable()
           {
               @Override
               public void run()
               {
                  GalaxyServer galaxy =  Jump.API.getServerGalaxy();
                  if(galaxy != null)
                  {
                     galaxy.handleClientStatus(message, player);
                  }
                  else
                  {
                     Jump.LOGGER.warn("GalaxyClientStatus Message received without GalaxyServer available");
                  }
               }
           });

           return null;
       }
   }
   
   private Type msgType;

   public MessageGalaxyClientStatus()
   {}
   
   public MessageGalaxyClientStatus(Type msgType)
   {
      this.msgType = msgType;
   }
   
   @Override
   public void fromBytes(ByteBuf buf)
   {
      byte ordinal = buf.readByte();
      if(ordinal >= 0 && ordinal < Type.values().length)
      {
         msgType = Type.values()[ordinal];
      }
      else
      {
         Jump.LOGGER.error("Cannot read " + MessageGalaxyClientStatus.class + " from network packet");
      }
   }

   @Override
   public void toBytes(ByteBuf buf)
   {
      if(Type.values().length < Byte.MAX_VALUE)
      {
         buf.writeByte(msgType.ordinal());
      }
      else
      {
         Jump.LOGGER.error("Cannot convert " + MessageGalaxyClientStatus.class + " to network packet");
      }
   }
   
   public Type getMsgType()
   {
      return msgType;
   }
}
