package alcubierre.jump.network;

import alcubierre.jump.api.GalaxyVector;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkHandler extends SimpleNetworkWrapper
{
   public NetworkHandler(String channelName)
   {
      super(channelName);
   }
   
   public void sendToAllInterested(GalaxyVector galaxyPosition)
   {
      
   }
   
   public void register()
   {
      int msgId = 0;
      registerMessage(MessageGalaxyClientStatus.Handler.class, MessageGalaxyClientStatus.class, msgId++, Side.SERVER);
      registerMessage(MessageGalaxyServerUpdate.Handler.class, MessageGalaxyServerUpdate.class, msgId++, Side.CLIENT);
   }
}
