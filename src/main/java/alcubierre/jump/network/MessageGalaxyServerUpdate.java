package alcubierre.jump.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import alcubierre.jump.Jump;
import alcubierre.jump.client.GalaxyClient;
import alcubierre.jump.galaxy.Galaxy;
import alcubierre.jump.galaxy.sector.Sector;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageGalaxyServerUpdate implements IMessage
{
   public static class Handler implements IMessageHandler<MessageGalaxyServerUpdate, IMessage>
   {
      @Override
      public IMessage onMessage(final MessageGalaxyServerUpdate msg, MessageContext ctx)
      {
         Minecraft.getMinecraft().addScheduledTask(new Runnable()
            {
               @Override
               public void run()
               {
                  GalaxyClient galaxy =  Jump.API.getClientGalaxy();
                  if(galaxy != null)
                  {
                     if(msg.updatedChunks != null)
                     {
                        galaxy.handleServerUpdate(msg);
                     }
                  }
                  else
                  {
                     Jump.LOGGER.warn("GalaxyServerUpdate Message received without GalaxyClient available");
                  }
               }
           });

         return null;
      }
   }

   Collection<Sector> updatedChunks;
   
   public MessageGalaxyServerUpdate()
   {}

   public MessageGalaxyServerUpdate(Collection<Sector> chunks)
   {
      updatedChunks = chunks;
   }
   
   @Override
   public void fromBytes(ByteBuf buf)
   {
      try
      {
         updatedChunks = new ArrayList<>();
         final int numOfChunks = buf.readInt();
         for(int chunkNum = 0; chunkNum < numOfChunks; ++chunkNum)
         {
            updatedChunks.add(new Sector(buf));
         }
      }
      // TODO: be more discriminating on caught exceptions
      catch(Exception e)
      {
         Jump.LOGGER.error(e);
         Jump.LOGGER.warn("Cannot read " + MessageGalaxyServerUpdate.class + " from network packet");
      }
   }

   @Override
   public void toBytes(ByteBuf buf)
   {
      buf.writeInt(updatedChunks.size());
      for(Sector chunk : updatedChunks)
      {
         chunk.toBytes(buf);
      }
      //Jump.LOGGER.error("Cannot convert " + MessageGalaxyServerUpdate.class + " to network packet");
   }
   
   public Collection<Sector> getUpdatedChunks()
   {
      return updatedChunks;
   }
}
