package alcubierre.jump.network;

import io.netty.buffer.ByteBuf;

/**
 * Intended to provide a common way to (de)serialize objects for use in packets
 * 
 * However, doing it like this doesn't allow for deserializing final fields. That
 *    would require constructing the object from the serialzed buffer.
 *    There is no real good way to do that without extending a base class or some kind of abstract factory 
 *
 */
public interface IMessageComponent
{
   /** Convert from the supplied buffer into your specific message type
   *
   * @param buf
   * @post clears the isDirty flag
   */
   void fromBytes(ByteBuf buf);

   /**
    * Deconstruct your message into the supplied byte buffer
    * @param buf
    * @post clears the isDirty flag
    */
   void toBytes(ByteBuf buf);
}