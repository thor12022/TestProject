package alcubierre.jump;

import alcubierre.jump.client.gui.GuiHandler;
import alcubierre.jump.config.ConfigData;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class CommonProxy
{
   public void preInit()
   {
      Jump.CONFIG.register(ConfigData.class);
      Jump.NETWORK.register();

      Jump.metals.createMetalItems();
      Jump.machines.createItems();
   }

   public void init()
   {
      Jump.LOGGER.info("COMMON PROXY INIT");
      Jump.metals.registerMetalRecipes();
      Jump.machines.registerRecipes();
      NetworkRegistry.INSTANCE.registerGuiHandler(Jump.instance, new GuiHandler());
   }

   protected void construct()
   {

   }

   public void load()
   {

   }

   public void postInit()
   {

   }

}
