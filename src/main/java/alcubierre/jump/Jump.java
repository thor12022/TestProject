package alcubierre.jump;

import alcubierre.jump.client.gui.GuiHandler;
import alcubierre.jump.machines.Machines;
import net.minecraft.creativetab.CreativeTabs;

import java.lang.reflect.Field;
import java.util.Random;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import alcubierre.jump.client.gui.CreativeTabJump;
import alcubierre.jump.config.ConfigManager;
import alcubierre.jump.metals.Metals;
import alcubierre.jump.network.NetworkHandler;
import alcubierre.jump.util.Reference;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

@Mod(	modid = ModInformation.ID, name = ModInformation.NAME,
		version = ModInformation.VERSION,
		guiFactory = ModInformation.MOD_GUIFACTORY_CLASS,
		clientSideOnly = true,
		updateJSON = Reference.ForgeVersionURL,
		acceptedMinecraftVersions = "@ACCEPTED_MC_VERSION@")
public class Jump
{
   public static GuiHandler guiHandler = new GuiHandler();

   @Instance(ModInformation.ID)
   public static Jump instance;

   @SidedProxy(clientSide = ModInformation.CLIENT_PROXY_CLASS)
   public static CommonProxy proxy;

   public static final CreativeTabs    CREATIVE_TAB   =  new CreativeTabJump("Jump");
   public static final Logger          LOGGER         =  LogManager.getLogger(ModInformation.ID);
   public static final ConfigManager   CONFIG         =  new ConfigManager(ModInformation.ID);
   public static final Random          RAND           =  new Random();
   public static final Mw              API            =  new Mw();
   public static final NetworkHandler  NETWORK        =  new NetworkHandler(ModInformation.CHANNEL);
	
	  public static Metals metals = new Metals();
	  public static Machines machines = new Machines();
	
	  public Jump()
   {
      try
      {
         Field f = alcubierre.jump.api.Jump.class.getField("instance");
         f.setAccessible(true);
         f.set(null, API);
         f.setAccessible(false);
      }
      catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
      {
         LOGGER.error(e);
         LOGGER.fatal("GOSH DANGIT PHTEVE");
      }
   }

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(this);
		proxy.preInit();
	}

	@EventHandler
 public void init(FMLInitializationEvent event)
 {
    proxy.init();
 }

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		proxy.load();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit();
	}

	@EventHandler
	public void serverAbouttoStart(FMLServerAboutToStartEvent event)
	{
	   API.initServer();
	}	
}
