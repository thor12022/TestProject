package alcubierre.jump.machines.alloyer;

import alcubierre.jump.Jump;
import alcubierre.jump.ModInformation;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

public class GUIAlloyer extends GuiContainer
{
   public static final int WIDTH = 176;
   public static final int HEIGHT = 166;

   private static final int FLAME_ICON_XPOS = 177;
   private static final int FLAME_ICON_YPOS = 0;

   private static final int BLANK_FLAME_XPOS = 81;
   private static final int BLANK_FLAME_YPOS = 26;

   private static final int FLAME_ICON_SIZEX = 14;
   private static final int FLAME_ICON_SIZEY = 14;

   private TileAlloyer tileAlloyer;

   private static final ResourceLocation background = new ResourceLocation(ModInformation.ID, "textures/gui/alloyer.png");

   public GUIAlloyer(TileAlloyer tileEntity, ContainerAlloyer container) {
      super(container);

      tileAlloyer = tileEntity;

      xSize = WIDTH;
      ySize = HEIGHT;
   }

   @Override
   protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
      mc.getTextureManager().bindTexture(background);
      drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

      //This CAN be done with a full 14 pixel fidelity, but, man, does it look funny
      int flameHeight = 2 * tileAlloyer.calculateRoundedNumberForFlame(FLAME_ICON_SIZEY / 2);

      //Draw any necessary flame over blank background
      drawTexturedModalRect((guiLeft + BLANK_FLAME_XPOS), (guiTop + BLANK_FLAME_YPOS + FLAME_ICON_SIZEY - flameHeight),
              FLAME_ICON_XPOS, FLAME_ICON_YPOS + FLAME_ICON_SIZEY - flameHeight, FLAME_ICON_SIZEX, flameHeight);

   }

   @Override
   protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
   {

   }

}
