package alcubierre.jump.machines.alloyer;

import alcubierre.jump.Jump;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nullable;

public class ContainerAlloyer extends Container
{
   final int SLOT_XSIZE = 18;
   final int SLOT_YSIZE = 18;
   final int LEFT_BUFFER = 8;
   final int TOP_OF_HOTBAR_BUFFER = 4;
   final int TOP_OF_PLAYER_INVENTORY_BUFFER = 84;
   final int BUFFER_OVER_HOTBAR = (3 * SLOT_YSIZE) + TOP_OF_HOTBAR_BUFFER + TOP_OF_PLAYER_INVENTORY_BUFFER;

   final int FUEL_XLOC = 80;
   final int FUEL_YLOC = 41;

   final int UPGRADE_XLOC = 16;
   final int UPGRADE_YLOC = 16;

   private TileAlloyer te;

   private int [] cachedFields;

   public ContainerAlloyer(IInventory playerInventory, TileAlloyer te) {
      this.te = te;

      // This container references items out of our own inventory (the 9 slots we hold ourselves)
      // as well as the slots from the player inventory so that the user can transfer items between
      // both inventories. The two calls below make sure that slots are defined for both inventories.
      addOwnSlots();
      addPlayerSlots(playerInventory);
   }

   private void addPlayerSlots(IInventory playerInventory) {
      // Slots for the main inventory
      for (int row = 0; row < 3; ++row) {
         for (int col = 0; col < 9; ++col) {
            int x = LEFT_BUFFER + (col * SLOT_XSIZE);
            int y = (row * SLOT_YSIZE) + TOP_OF_PLAYER_INVENTORY_BUFFER;
            this.addSlotToContainer(new Slot(playerInventory, col + row * 9 + 10, x, y));
         }
      }

      // Slots for the hotbar
      for (int row = 0; row < 9; ++row) {
         int x = LEFT_BUFFER + (row * SLOT_XSIZE);
         int y = BUFFER_OVER_HOTBAR;
         this.addSlotToContainer(new Slot(playerInventory, row, x, y));
      }
   }

   private void addOwnSlots() {
      IItemHandler itemHandler = this.te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

      addSlotToContainer(new SlotItemHandler(itemHandler, te.FUEL_SLOT, FUEL_XLOC, FUEL_YLOC));
      addSlotToContainer(new SlotItemHandler(itemHandler, te.UPGRADE_SLOT, UPGRADE_XLOC, UPGRADE_YLOC));

   }

   @Nullable
   @Override
   public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
      ItemStack itemstack = null;
      Slot slot = this.inventorySlots.get(index);

      if (slot != null && slot.getHasStack()) {
         ItemStack itemstack1 = slot.getStack();
         itemstack = itemstack1.copy();

         if (index < TileAlloyer.SIZE) {
            if (!this.mergeItemStack(itemstack1, TileAlloyer.SIZE, this.inventorySlots.size(), true)) {
               return null;
            }
         } else if (!this.mergeItemStack(itemstack1, 0, TileAlloyer.SIZE, false)) {
            return null;
         }

         if (itemstack1.stackSize == 0) {
            slot.putStack(null);
         } else {
            slot.onSlotChanged();
         }
      }

      return itemstack;
   }

   @Override
   public boolean canInteractWith(EntityPlayer playerIn) {
      return te.canInteractWith(playerIn);
   }


}
