package alcubierre.jump.machines.alloyer;

import java.util.Random;

import alcubierre.jump.Jump;
import alcubierre.jump.ModInformation;
import alcubierre.jump.client.gui.GuiHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.ShapedOreRecipe;

import javax.annotation.Nullable;

public class BlockAlloyer extends Block implements ITileEntityProvider
{

   public static final PropertyDirection FACING = BlockHorizontal.FACING;
   public static final PropertyBool ACTIVE = PropertyBool.create("active");
   public static final PropertyInteger CURRENT_ENERGY = PropertyInteger.create("current_energy", 0, 6);

   private boolean isActive;

   public BlockAlloyer()
   {
      super(Material.ROCK);
      setUnlocalizedName(ModInformation.ID + ".alloyer");
      this.setRegistryName(ModInformation.ID, "alloyer");

      setCreativeTab(Jump.CREATIVE_TAB);
      this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
      setHardness(3.5f);
   }

   @Override
   public TileEntity createNewTileEntity(World world, int meta)
   {
      return new TileAlloyer();
   }

   @Override
   public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand,
                                   @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
   {
      // Only execute on the server
      if (world.isRemote) {
         return true;
      }
      playerIn.openGui(Jump.instance, GuiHandler.GENERATOR, world, pos.getX(), pos.getY(), pos.getZ());
      return true;
   }

   @Override
   public void onBlockAdded(World world, BlockPos pos, IBlockState state)
   {
      isActive = false;
      if (!world.isRemote)
      {
         IBlockState iblockstate = world.getBlockState(pos.north());
         IBlockState iblockstate1 = world.getBlockState(pos.south());
         IBlockState iblockstate2 = world.getBlockState(pos.west());
         IBlockState iblockstate3 = world.getBlockState(pos.east());
         EnumFacing enumfacing = (EnumFacing)state.getValue(FACING);

         if (enumfacing == EnumFacing.NORTH && iblockstate.isFullBlock() && !iblockstate1.isFullBlock())
         {
            enumfacing = EnumFacing.NORTH;
         }
         else if (enumfacing == EnumFacing.SOUTH && iblockstate1.isFullBlock() && !iblockstate.isFullBlock())
         {
            enumfacing = EnumFacing.SOUTH;
         }
         else if (enumfacing == EnumFacing.WEST && iblockstate2.isFullBlock() && !iblockstate3.isFullBlock())
         {
            enumfacing = EnumFacing.WEST;
         }
         else if (enumfacing == EnumFacing.EAST && iblockstate3.isFullBlock() && !iblockstate2.isFullBlock())
         {
            enumfacing = EnumFacing.EAST;
         }

         world.setBlockState(pos, state.withProperty(FACING, enumfacing).
                 withProperty(ACTIVE, isActive).
                 withProperty(CURRENT_ENERGY, 0), 2);
      }
   }

   /**
    * Called by ItemBlocks just before a block is actually set in the world, to allow for adjustments to the
    * IBlockstate
    */
   public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing,
                                    float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
   {
      return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing().getOpposite());
   }

   /**
    * Called by ItemBlocks after a block is set in the world, to allow post-place logic
    */
   public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
   {
      worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing().getOpposite()), 2);
   }

   @Override
   @SideOnly(Side.CLIENT)
   public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random random)
   {
      TileEntity te = world.getTileEntity(pos);

      if(te instanceof TileAlloyer)
      {
         //We actual call the TE to get the "actual block", so this is probably unnecessary
         world.notifyBlockUpdate( pos, stateIn, ((TileAlloyer)te).getNewBlockState(), 3);
         world.notifyBlockOfStateChange(pos, this);
      }
   }

   protected BlockStateContainer createBlockState()
   {
      return new BlockStateContainer(this, new IProperty[] {ACTIVE, FACING, CURRENT_ENERGY});
   }

   @Override
   public int getMetaFromState(IBlockState state)
   {

      return state.getValue(FACING).getIndex()-2;
   }

   @Override
   public IBlockState getStateFromMeta(int meta)
   {
      return getDefaultState().withProperty(FACING, EnumFacing.getFront((meta & 3) + 2));
   }

   @Override
   public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos)
   {
      TileEntity te = world.getTileEntity(pos);

      if(te instanceof TileAlloyer)
      {
         return ((TileAlloyer)te).getNewBlockState();
      }
      return state;

   }

   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(ModInformation.ID + ":machines/alloyer"));
   }

   public void registerRecipe()
   {
      GameRegistry.addRecipe(new ShapedOreRecipe(Item.getItemFromBlock(this),
              "IRI",
              "IfI",
              "III",
              'I', Item.getItemFromBlock(Blocks.IRON_BLOCK),
              'f', Item.getItemFromBlock(Blocks.FURNACE),
              'R', Item.getItemFromBlock(Blocks.REDSTONE_BLOCK)));
   }
}
