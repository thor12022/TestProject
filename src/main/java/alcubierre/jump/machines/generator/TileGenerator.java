package alcubierre.jump.machines.generator;

import alcubierre.jump.config.Config;
import alcubierre.jump.config.Configurable;
import alcubierre.jump.machines.TileMachineBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.energy.IEnergyStorage;

import static alcubierre.jump.machines.generator.BlockGenerator.ACTIVE;
import static alcubierre.jump.machines.generator.BlockGenerator.CURRENT_ENERGY;

@Configurable
public class TileGenerator extends TileMachineBase implements ITickable, IEnergyStorage {

   public static final int FUEL_SLOT = 1;
   public static final int SIZE = 2;

   @Config
   public static final int MAX_ENERGY_STORAGE = 10000;
   @Config
   public static final int ENERGY_PER_TICK = 20;
   @Config
   public static final int MAX_ENERGY_TO_OUTPUT = 32;


   @Override
   public void update()
   {
      super.update();

      ItemStack stack = itemStackHandler.getStackInSlot(FUEL_SLOT);
      if (currentProcessTimer > 0)
      {
         currentProcessTimer --;
         if (currentEnergy < MAX_ENERGY_STORAGE)
         {
            currentEnergy += ENERGY_PER_TICK;
         }
         if(!isActive)
         {
            isActive = true;
            forceUpdate();
         }
      }
      else if ((currentProcessTimer <= 0) && (currentEnergy < MAX_ENERGY_STORAGE) && (stack != null))
      {
         currentProcessTimerMax = getItemBurnTime(stack);

         if (currentProcessTimerMax != 0)
         {
            isActive = true;
            itemStackHandler.extractItem(FUEL_SLOT, 1, false);
            currentProcessTimer = currentProcessTimerMax;
            forceUpdate();
         }
      }
      else
      {
         if(isActive)
         {
            isActive = false;
            forceUpdate();  //if we JUST went inactive, force an update
         }
      }
   }

   int calculateRoundedNumberForFlame(int divisorNumber)
   {
      double burnTime = MathHelper.clamp_double(((double)currentProcessTimer / (double)currentProcessTimerMax), 0.0, 1.0);
      burnTime = burnTime * divisorNumber;
      return MathHelper.ceiling_double_int(burnTime);
   }

   @Override
   public int getItemStackSize()
   {
      return SIZE;
   }

   // ************************ Energy Implementations ************************

   @Override
   public int receiveEnergy(int maxReceive, boolean simulate)
   {
      return 0;
   }

   @Override
   public int extractEnergy(int maxExtract, boolean simulate)
   {
      int retValue = maxExtract;
      if(MAX_ENERGY_TO_OUTPUT < retValue)
      {
         retValue = MAX_ENERGY_TO_OUTPUT;
      }
      if(currentEnergy < retValue)
      {
         retValue = currentEnergy;
      }

      currentEnergy -= retValue;
      return retValue;
   }

   @Override
   public int getMaxEnergyStored()
   {
      return MAX_ENERGY_STORAGE;
   }

   @Override
   public boolean canExtract()
   {
      return true;
   }





   private static int getItemBurnTime(ItemStack stack)
   {
      if (stack == null)
      {
         return 0;
      }
      else
      {
         Item item = stack.getItem();

         if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.AIR)
         {
            Block block = Block.getBlockFromItem(item);

            if (block == Blocks.WOODEN_SLAB)
            {
               return 150;
            }

            if (block.getDefaultState().getMaterial() == Material.WOOD)
            {
               return 300;
            }

            if (block == Blocks.COAL_BLOCK)
            {
               return 16000;
            }
         }

         if (item instanceof ItemTool && "WOOD".equals(((ItemTool)item).getToolMaterialName())) return 200;
         if (item instanceof ItemSword && "WOOD".equals(((ItemSword)item).getToolMaterialName())) return 200;
         if (item instanceof ItemHoe && "WOOD".equals(((ItemHoe)item).getMaterialName())) return 200;
         if (item == Items.STICK) return 100;
         if (item == Items.COAL) return 1600;
         if (item == Items.LAVA_BUCKET) return 20000;
         if (item == Item.getItemFromBlock(Blocks.SAPLING)) return 100;
         if (item == Items.BLAZE_ROD) return 2400;
         return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(stack);
      }
   }

}