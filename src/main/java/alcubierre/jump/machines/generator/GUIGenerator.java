package alcubierre.jump.machines.generator;

import alcubierre.jump.Jump;
import alcubierre.jump.ModInformation;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

public class GUIGenerator extends GuiContainer
{
   public static final int WIDTH = 176;
   public static final int HEIGHT = 166;

   private static final int FLAME_ICON_XPOS = 177;
   private static final int FLAME_ICON_YPOS = 0;

   private static final int BLANK_FLAME_XPOS = 81;
   private static final int BLANK_FLAME_YPOS = 26;

   private static final int FLAME_ICON_SIZEX = 14;
   private static final int FLAME_ICON_SIZEY = 14;

   private TileGenerator tileGenerator;

   private static final ResourceLocation background = new ResourceLocation(ModInformation.ID, "textures/gui/generator.png");

   public GUIGenerator(TileGenerator tileEntity, ContainerGenerator container) {
      super(container);

      tileGenerator = tileEntity;

      xSize = WIDTH;
      ySize = HEIGHT;
   }

   @Override
   protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
      mc.getTextureManager().bindTexture(background);
      drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

      //This CAN be done with a full 14 pixel fidelity, but, man, does it look funny
      int flameHeight = 2 * tileGenerator.calculateRoundedNumberForFlame(FLAME_ICON_SIZEY / 2);

      //Draw any necessary flame over blank background
      drawTexturedModalRect((guiLeft + BLANK_FLAME_XPOS), (guiTop + BLANK_FLAME_YPOS + FLAME_ICON_SIZEY - flameHeight),
              FLAME_ICON_XPOS, FLAME_ICON_YPOS + FLAME_ICON_SIZEY - flameHeight, FLAME_ICON_SIZEX, flameHeight);

   }

   @Override
   protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
   {

   }

}
