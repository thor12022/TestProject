package alcubierre.jump.machines;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraft.tileentity.*;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import static alcubierre.jump.machines.alloyer.BlockAlloyer.ACTIVE;
import static alcubierre.jump.machines.alloyer.BlockAlloyer.CURRENT_ENERGY;

public class TileMachineBase extends TileEntity implements ITickable, IEnergyStorage
{
   public static final int UPGRADE_SLOT = 0;

   protected boolean isActive = false;
   protected int currentProcessTimer = 0;
   protected int currentEnergy = 0;
   protected int currentProcessTimerMax = 0;
   protected int currentUpgradeSize = 0;
   protected int currentEnergyForDisplay = 0;

   protected ItemStackHandler itemStackHandler = new ItemStackHandler(getItemStackSize()) {
      @Override
      protected void onContentsChanged(int slot) {
         // We need to tell the tile entity that something has changed so
         // that the chest contents is persisted
         TileMachineBase.this.markDirty();
      }
   };

   public boolean canInteractWith(EntityPlayer playerIn) {
      // If we are too far away from this tile entity you cannot use it
      return !isInvalid() && playerIn.getDistanceSq(pos.add(0.5D, 0.5D, 0.5D)) <= 64D;
   }

   @Override
   public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
      if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
         return true;
      }
      return super.hasCapability(capability, facing);
   }

   @Override
   public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
      if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
         return (T) itemStackHandler;
      }
      return super.getCapability(capability, facing);
   }

   @Override
   public void update()
   {


      int newEnergyForDisplay = calculateRoundedNumberForDisplay(currentUpgradeSize);
      if(newEnergyForDisplay != currentEnergyForDisplay)
      {
         currentEnergyForDisplay = newEnergyForDisplay;
         forceUpdate();
      }

      ItemStack stack = itemStackHandler.getStackInSlot(UPGRADE_SLOT);
      if (stack != null)
      {
         if(stack.getItem() == Item.getItemFromBlock(Blocks.REDSTONE_LAMP))
         {
            int newUpgradeSize = stack.stackSize;
            if(newUpgradeSize > 6)
            {
               newUpgradeSize = 6;
            }
            if (newUpgradeSize != currentUpgradeSize)
            {
               currentUpgradeSize = newUpgradeSize;
               forceUpdate();
            }

         }

      }
      else
      {
         if(currentUpgradeSize != 0)
         {
            currentUpgradeSize = 0;
            forceUpdate();
         }

      }
   }

   @Override
   public NBTTagCompound getUpdateTag() {
      // getUpdateTag() is called whenever the chunkdata is sent to the
      // client. In contrast getUpdatePacket() is called when the tile entity
      // itself wants to sync to the client. In many cases you want to send
      // over the same information in getUpdateTag() as in getUpdatePacket().
      return writeToNBT(new NBTTagCompound());
   }

   @Override
   public SPacketUpdateTileEntity getUpdatePacket() {
      // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
      // and that's all we have we just write our entire NBT here. If you have a complex
      // tile entity that doesn't need to have all information on the client you can write
      // a more optimal NBT here.
      NBTTagCompound nbtTag = new NBTTagCompound();
      this.writeToNBT(nbtTag);
      return new SPacketUpdateTileEntity(getPos(), 1, nbtTag);
   }

   @Override
   public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet) {
      // Here we get the packet from the server and read it into our client side tile entity
      this.readFromNBT(packet.getNbtCompound());
   }

   @Override
   public void readFromNBT(NBTTagCompound compound) {
      super.readFromNBT(compound);
      if (compound.hasKey("items"))
      {
         itemStackHandler.deserializeNBT((NBTTagCompound) compound.getTag("items"));
      }
      if (compound.hasKey("currentProcessTimer"))
      {
         currentProcessTimer = compound.getInteger("currentProcessTimer");
      }
      if (compound.hasKey("currentEnergy"))
      {
         currentEnergy = compound.getInteger("currentEnergy");
      }
      if (compound.hasKey("currentProcessTimerMax"))
      {
         currentProcessTimerMax = compound.getInteger("currentProcessTimerMax");
      }
      if (compound.hasKey("isActive"))
      {
         isActive = compound.getBoolean("isActive");
      }
      forceUpdate();
   }

   @Override
   public NBTTagCompound writeToNBT(NBTTagCompound compound) {
      super.writeToNBT(compound);
      compound.setTag("items", itemStackHandler.serializeNBT());

      compound.setInteger("currentProcessTimer", currentProcessTimer);
      compound.setInteger("currentEnergy", currentEnergy);
      compound.setInteger("currentProcessTimerMax", currentProcessTimerMax);
      compound.setBoolean("isActive", isActive);

      return compound;
   }

   private int calculateRoundedNumberForDisplay(int divisorNumber)
   {
      double energy = MathHelper.clamp_double(((double)currentEnergy / (double)getMaxEnergyStored()), 0.0, 1.0);
      energy = energy * divisorNumber;
      return MathHelper.ceiling_double_int(energy);
   }
   protected void forceUpdate()
   {
      getUpdatePacket();
   }

   public IBlockState getNewBlockState()
   {
      return worldObj.getBlockState(getPos()).withProperty(ACTIVE, isActive)
              .withProperty(CURRENT_ENERGY, currentEnergyForDisplay);
   }

   public int getItemStackSize()
   {
      return 0;
   }

   // ************************ Energy Implementations ************************

   @Override
   public int receiveEnergy(int maxReceive, boolean simulate)
   {
      return 0;
   }

   @Override
   public int extractEnergy(int maxExtract, boolean simulate)
   {
      return 0;
   }

   @Override
   public int getEnergyStored()
   {
      return currentEnergy;
   }

   @Override
   public int getMaxEnergyStored()
   {
      return 0;
   }

   @Override
   public boolean canExtract()
   {
      return false;
   }

   @Override
   public boolean canReceive()
   {
      return false;
   }
}