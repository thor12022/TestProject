package alcubierre.jump.machines;

import alcubierre.jump.ModInformation;
import alcubierre.jump.machines.alloyer.BlockAlloyer;
import alcubierre.jump.machines.alloyer.TileAlloyer;
import alcubierre.jump.machines.generator.BlockGenerator;
import alcubierre.jump.machines.generator.TileGenerator;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by audiomodder on 9/9/2016.
 */
public class Machines
{
   private BlockGenerator blockGenerator;
   private TileGenerator tileGenerator;

   //private BlockAlloyer blockAlloyer;
   //private TileAlloyer tileAlloyer;



   public Machines()
   {
      blockGenerator = new BlockGenerator();
      tileGenerator = new TileGenerator();

      //blockAlloyer = new BlockAlloyer();
      //tileAlloyer = new TileAlloyer();
   }

   public void createItems()
   {
      GameRegistry.register(blockGenerator);
      GameRegistry.register(new ItemBlock(blockGenerator).setRegistryName(blockGenerator.getRegistryName()));
      GameRegistry.registerTileEntity(TileGenerator.class, blockGenerator.getUnlocalizedName());

      //GameRegistry.register(blockAlloyer);
      //GameRegistry.register(new ItemBlock(blockAlloyer).setRegistryName(blockAlloyer.getRegistryName()));
      //GameRegistry.registerTileEntity(TileAlloyer.class, blockAlloyer.getUnlocalizedName());

   }

   public void registerItems()
   {
      blockGenerator.registerModel();
      //blockAlloyer.registerModel();
   }

   public void registerRecipes()
   {
      blockGenerator.registerRecipe();
      //blockAlloyer.registerRecipe();
   }
}
