package alcubierre.jump.config;

import alcubierre.jump.Jump;

@Configurable(sectionName="options")
public class ConfigData
{
	public static final String[] backgroundModeStringArray =
	{
			"mw.config.backgroundTextureMode.none",
			"mw.config.backgroundTextureMode.static",
			"mw.config.backgroundTextureMode.panning"
	};

	// configuration options
	@Config
	public static boolean linearTextureScaling = true;

	@Config
   public static boolean teleportEnabled = true;
	
	@Config
   public static String teleportCommand = "tp";

   @Config(minInt = 1, maxInt = 256)
	public static int zoomOutLevels = 5;

   @Config(minInt = 0, maxInt = 256)
   public static int zoomInLevels = 5;

   @Config
   public static boolean useSavedBlockColours = false;
      
   @Config(maxInt = 256 * 256, minInt = 1)
	public static int maxChunkSaveDistSq = 128 * 128;

   @Config
	public static boolean mapPixelSnapEnabled = true;

   @Config(minInt = 1024, maxInt = 4096)
   public static int configTextureSize = 2048;

   @Config(minInt = 0, maxInt = 1000)
   public static int maxDeathMarkers = 3;

   @Config(minInt = 1, maxInt = 500)
   public static int chunksPerTick = 5;

   @Config
   public static boolean portNumberInWorldNameEnabled = true;

   @Config
   public static String saveDirOverride = "";

   @Config
   public static boolean regionFileOutputEnabledSP = true;

   @Config
   public static boolean regionFileOutputEnabledMP = true;

   @Config
   public static String backgroundTextureMode = backgroundModeStringArray[0];

   @Config
   public static boolean moreRealisticMap = false;

	// World configuration Options

   @Config(minInt = 0, maxInt = 1000, showInGui = false)
   public static int overlayModeIndex = 0;

   @Config(minInt = -5, maxInt = 5, showInGui = false)
   public static int overlayZoomLevel = 0;

   @Config
   public static int fullScreenZoomLevel = 0;

	public static MapModeConfig fullScreenMap = new MapModeConfig();
	
	static
	{
	   Jump.CONFIG.getCategory(ConfigData.class).setShowInGui(true);
	   
	   Jump.CONFIG.getConfig()
         .get(Jump.CONFIG.getCategory(ConfigData.class).getQualifiedName(), "overlayModeIndex", ConfigData.overlayModeIndex)
         .setShowInGui(false);
	   
	   Jump.CONFIG.getConfig()
         .get(Jump.CONFIG.getCategory(ConfigData.class).getQualifiedName(), "overlayZoomLevel", ConfigData.zoomInLevels)
         .setShowInGui(false);
	}
}
