package alcubierre.jump.config;

import alcubierre.jump.Jump;
import alcubierre.jump.api.client.IMapModeConfig;
import alcubierre.jump.client.gui.ModGuiConfig.ModNumberSliderEntry;
import alcubierre.jump.client.gui.ModGuiConfigHUD.MapPosConfigEntry;
import alcubierre.jump.util.Reference;
import net.minecraftforge.common.config.Configuration;

@Configurable(sectionName="map")
public class MapModeConfig implements IMapModeConfig
{
	public static final String[] COORDS_MODE_STRING_ARRAY =
	{
			"mw.config.map.coordsMode.disabled",
			"mw.config.map.coordsMode.small",
			"mw.config.map.coordsMode.large"
	};

   @Config
   public boolean enabled = true;

   @Config
   public boolean circular = false;
   
   @Config
   public String coordsMode = COORDS_MODE_STRING_ARRAY[0];

   @Config
   public boolean borderMode = false;

   @Config(minInt = 1, maxInt = 20)
   public int playerArrowSize = 5;

   @Config(minInt = 1, maxInt = 20)
   public int markerSize = 5;

   @Config(minInt = 1, maxInt = 100)
   public int alphaPercent = 100;

   @Config
   public String biomeMode = COORDS_MODE_STRING_ARRAY[0];

   @Config(subSection = "mappos", minFloat = 0, maxFloat = 100)
   public float xPos = 0;

   @Config(subSection = "mappos", minFloat = 0, maxFloat = 100)
   public float yPos = 0;

   @Config(subSection = "mappos", minFloat = 0, maxFloat = 100)
   public float heightPercent = 100;

   @Config(subSection = "mappos", minFloat = 0, maxFloat = 100)
   public float widthPercent = 100;

	public MapModeConfig()
	{
		Jump.CONFIG.register(this);
	}

	public void setDefaults()
	{
		Jump.CONFIG.getCategory(this)
				.setLanguageKey("mw.config.map.ctgy.position")
				.setConfigEntryClass(MapPosConfigEntry.class)
				.setShowInGui(false);
	}

	@Override
	public String[] getCoordsModeStringArray()
	{
		return COORDS_MODE_STRING_ARRAY;
	}

	@Override
	public boolean getEnabled()
	{
		return this.enabled;
	}

	@Override
	public boolean getCircular()
	{
		return this.circular;
	}

	@Override
	public String getCoordsMode()
	{
		return this.coordsMode;
	}

	@Override
	public boolean getBorderMode()
	{
		return this.borderMode;
	}

	@Override
	public int getPlayerArrowSize()
	{
		return this.playerArrowSize;
	}

	@Override
	public int getMarkerSize()
	{
		return this.markerSize;
	}

	@Override
	public int getAlphaPercent()
	{
		return this.alphaPercent;
	}

	@Override
	public String getBiomeMode()
	{
		return this.biomeMode;
	}

	@Override
	public double getXPos()
	{
		return this.xPos;
	}

	@Override
	public double getYPos()
	{
		return this.yPos;
	}

	@Override
	public double getHeightPercent()
	{
		return this.heightPercent;
	}

	@Override
	public double getWidthPercent()
	{
		return this.widthPercent;
	}
}