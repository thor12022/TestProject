package alcubierre.jump.config;

import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import alcubierre.jump.Jump;
import alcubierre.jump.ModInformation;

public class ConfigManager
{   
   final private ArrayList<Object> configObjects = new ArrayList<>();
   final private ArrayList<Class<?>> configClasses = new ArrayList<>();
   final private Configuration configuration;
   
   public ConfigManager(String modName)
   {
      File config = new File(Loader.instance().getConfigDir().getAbsolutePath() + File.separatorChar + 
                             modName + ".cfg");
      configuration = new Configuration(config);
   }
   
   /**
    * Register a class instance for configuration
    * @param target the object that contains the configurable fields.
    *    Must use the @Configurable annotation
    */
   public void register(Object target)
   {
      if(target instanceof Class)
      {
         Configurable annotation = ((Class<?>)target).getAnnotation(Configurable.class);
         if(annotation != null)
         {
            try
            {
               Class.forName(((Class<?>) target).getName());
               configClasses.add(((Class<?>)target));
               processConfigClass(((Class<?>)target));
               configuration.save();
            }
            catch(ClassNotFoundException e)
            {
               Jump.LOGGER.error(e);
            }
         }
      }
      else
      {
         Configurable annotation = target.getClass().getAnnotation(Configurable.class);
         if(annotation != null)
         {
            configObjects.add(target);
            processConfigObject(target);
            configuration.save();
         }
      }
   }

   public void syncConfig(Object target)
   {
      if(configuration != null)
      {
         if(target instanceof Class)
         {
            processConfigClass((Class<?>)target);
         }
         else
         {
            processConfigObject(target);
         }
         configuration.save();
         processNotifications(target);
      }
   }
   
   public void syncConfig()
   {
      if(configuration != null)
      {
         for(Object configObject : configObjects)
         {
            processConfigObject(configObject);
         }
         for(Class<?> configClass : configClasses)
         {
            processConfigClass(configClass);
         }
         configuration.save();
         for(Object configObject : configObjects)
         {
            processNotifications(configObject);
         }
         
         for(Class<?> configClass : configClasses)
         {
            processNotifications(configClass);
         }
      }
   }
   
   public File getConfigFile()
   {
      return configuration.getConfigFile();
   }
   
   public Configuration getConfig()
   {
      return configuration;
   }
   
   public ConfigCategory getCategory(Object target)
   {
      Configurable annotation;
      if(target instanceof Class)
      {
         annotation = ((Class<?>)target).getAnnotation(Configurable.class);
      }
      else
      {
         annotation = target.getClass().getAnnotation(Configurable.class);
      }
      if(annotation != null)
      {
         return configuration.getCategory(annotation.sectionName());
      }
      return null;
   }
   
   private void processConfigObject(Object target)
   {
      try
      {
         Configurable targetAnnotation = target.getClass().getAnnotation(Configurable.class);
         String sectionName = targetAnnotation.sectionName();
         if(sectionName.isEmpty())
         {
            sectionName = target.getClass().getSimpleName();
         }
         processClass(sectionName, target, target.getClass());
      }
      catch(Exception e)
      {
         Jump.LOGGER.warn(e);
         Jump.LOGGER.warn("Cannot find @Configurable Annotation on " + target.getClass().getName());
      }
   }
   
   private void processConfigClass(Class<?> target)
   {
      try
      {
         Configurable targetAnnotation = target.getAnnotation(Configurable.class);
         String sectionName = targetAnnotation.sectionName();
         if(sectionName.isEmpty())
         {
            sectionName = target.getSimpleName();
         }
         processClass(sectionName, null, target);
      }
      catch(Exception e)
      {
         Jump.LOGGER.warn(e);
         Jump.LOGGER.warn("Cannot find @Configurable Annotation on " + target.getName());
      }
   }
   
   private static void processNotifications(Object target)
   {
      Configurable targetAnnotation;
      if(target instanceof Class)
      {
         targetAnnotation = ((Class<?>)target).getAnnotation(Configurable.class);
      }
      else
      {
         targetAnnotation = target.getClass().getAnnotation(Configurable.class);
      }
      
      String callbackName = targetAnnotation.syncNotification();
      if(!callbackName.isEmpty())
      {
         try
         {
            Method method = target.getClass().getDeclaredMethod(callbackName);
            method.invoke(target);
         }
         catch(Exception excp)
         {
            Jump.LOGGER.debug(excp);
            Jump.LOGGER.warn("Invalid ConfigData callback method: " + target.getClass().getName() + "." + callbackName);
         }
      }
   }
   
   /**
    * Recurses over the class hierarchy of an object
    * @param sectionName name of the CONFIG section to write to
    * @param classObj object currently being processed 
    * @param currentClass the level in the classObj 's hierarchy being processed
    * @todo handler Config#showInGui()
    **/
   private void processClass(String sectionName, Object classObj, Class<?> currentClass)
   {
      if(currentClass == null || currentClass == Object.class)
      {
         return;
      }
      processClass(sectionName, classObj, currentClass.getSuperclass());
      for(Field field : currentClass.getDeclaredFields())
      {
         Config configAnnotation = field.getAnnotation(Config.class);
         if(configAnnotation != null)
         {
            String fieldName = configAnnotation.fieldName();
            if(fieldName.isEmpty())
            {
               fieldName = field.getName();
            }
            try
            {
               boolean isAccessible = field.isAccessible();
               field.setAccessible(true);
               final String completeSectionName = sectionName + (configAnnotation.subSection().isEmpty() ? "" : Configuration.CATEGORY_SPLITTER + configAnnotation.subSection());
               final String langKey = ModInformation.ID + ".config." + completeSectionName + "." + fieldName;
               if(field.getType() == boolean.class)
               {
                  field.set(classObj, configuration.getBoolean(fieldName, completeSectionName, field.getBoolean(classObj), configAnnotation.comment(), langKey));
               }
               else if(field.getType() == float.class)
               {
                  field.set(classObj, configuration.getFloat(fieldName, completeSectionName, field.getFloat(classObj), configAnnotation.minFloat(), configAnnotation.maxFloat(), configAnnotation.comment(), langKey));
               }
               else if(field.getType() == int.class)
               {
                  field.set(classObj, configuration.getInt(fieldName, completeSectionName, field.getInt(classObj), configAnnotation.minInt(), configAnnotation.maxInt(), configAnnotation.comment(), langKey));
               }
               else if(field.getType() == String.class)
               {
                  field.set(classObj, configuration.getString(fieldName, completeSectionName, (String)field.get(classObj), configAnnotation.comment(), langKey));
               }
               else
               {
                  processClass(sectionName, field.get(classObj), field.getType());
               }
               field.setAccessible(isAccessible);
            }
            catch(IllegalAccessException excp)
            {
               Jump.LOGGER.warn(excp);
               String extraError = classObj == null ? "Possibly nonstatic field used with static Class Registration" : "";
               Jump.LOGGER.warn("Problem getting configurable field \"" + currentClass.getName() + "." + field.getName() + "\"." + extraError); 
            }
         }
      }
   }  
}