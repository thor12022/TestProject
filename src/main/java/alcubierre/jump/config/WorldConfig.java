package alcubierre.jump.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import alcubierre.jump.Jump;
import alcubierre.jump.Mw;
import alcubierre.jump.util.Reference;
import alcubierre.jump.util.Utils;
import net.minecraftforge.common.config.Configuration;

public class WorldConfig
{
	private static WorldConfig instance = null;

	public Configuration worldConfiguration = null;


	private WorldConfig()
	{
		// load world specific config file
		File worldConfigFile = new File(Jump.API.worldDir, Reference.worldDirConfigName);
		this.worldConfiguration = new Configuration(worldConfigFile);
	}

	public static WorldConfig getInstance()
	{
		if (instance == null)
		{
			synchronized (WorldConfig.class)
			{
				if (instance == null)
				{
					instance = new WorldConfig();
				}
			}
		}

		return instance;
	}

	public void saveWorldConfig()
	{
		this.worldConfiguration.save();
	}
}
