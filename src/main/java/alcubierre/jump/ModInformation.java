package alcubierre.jump;

/*
 * Basic information your mod depends on.
 */

public class ModInformation
{

   public static final String ID = "jump";
   public static final String CHANNEL = "Jump";
   public static final String NAME = "Jump";
   public static final String VERSION = "@MOD_VERSION@";
   public static final String MOD_GUIFACTORY_CLASS = "alcubierre.jump.client.gui.ModGuiFactoryHandler";
   public static final String CLIENT_PROXY_CLASS = "alcubierre.jump.client.ClientProxy";
   public static final String SERVER_PROXY_CLASS = "alcubierre.jump.CommonProxy";
}
